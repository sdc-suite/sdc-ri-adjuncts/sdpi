plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.serialization)
    application
}

group = "org.somda.sdpi"
version = "1.0-SNAPSHOT"

val javaSource: String by project
val jdkVersion: String by project


repositories {
    mavenLocal()
    mavenCentral()
    // Coded Attribute Model
    maven("https://gitlab.com/api/v4/projects/43332997/packages/maven")
    // Coded Attribute
    maven("https://gitlab.com/api/v4/projects/43326846/packages/maven")
    // SDCri
    maven {
        url = uri("https://oss.sonatype.org/content/repositories/snapshots")
        mavenContent {
            snapshotsOnly()
        }
    }
}

dependencies {
    implementation(libs.bundles.kotlin)
    implementation(libs.glue)
    implementation(libs.biceps)
    implementation(libs.bundles.sdpi)
    implementation(libs.clikt)
    implementation(libs.bundles.bouncycastle)
    implementation(libs.bundles.log4j)
    implementation(libs.bundles.ktoml)
}

tasks.test {
    useJUnitPlatform()
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(javaSource)
    }
    withJavadocJar()
    withSourcesJar()
}

kotlin {
    jvmToolchain(jdkVersion.toInt())
}

application {
    mainClass = project.findProperty("chooseMain").toString()
}