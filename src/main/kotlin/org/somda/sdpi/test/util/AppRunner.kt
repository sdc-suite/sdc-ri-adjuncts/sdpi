package org.somda.sdpi.test.util

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.file
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.Filter
import org.apache.logging.log4j.core.appender.ConsoleAppender
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.biceps.model.participant.AbstractMetricValue
import org.somda.sdc.biceps.model.participant.AbstractMetricValue.MetricQuality
import org.somda.sdc.biceps.model.participant.GenerationMode
import org.somda.sdc.biceps.model.participant.MeasurementValidity
import org.somda.sdc.common.logging.InstanceLogger
import org.somda.sdc.dpws.crypto.CryptoSettings
import org.somda.sdc.glue.consumer.helper.HostingServiceLogger
import org.somda.sdpi.test.util.config.TlsConfig
import java.io.File
import java.util.function.Consumer

abstract class AppRunner() : CliktCommand() {
    val configFile by option("--config", help = "Path to a config TOML")
        .file()
        .default(File("app-config.toml"))

    val adapterIp by option("--adapter-ip", "-i", help = "IP address of the adapter")

    val epr by option("--epr-address", "-e", help = "EPR address of the provider")

    companion object : Logging {
        private val CHATTY_LOGGERS = listOf(
            "org.apache.http.wire",
            "org.apache.http.headers",
            "org.eclipse.jetty"
//            "org.somda.sdc.biceps.provider.access.LocalMdibAccessImpl",
//            "org.somda.sdc.biceps.common.storage.MdibStorageImpl"
        )

        private const val CUSTOM_PATTERN = ("%d{HH:mm:ss.SSS}"
                + " [%thread]" // only include the space if we have a variable for these
                + " %notEmpty{[%X{" + InstanceLogger.INSTANCE_ID + "}] }"
                + " %notEmpty{[%X{" + HostingServiceLogger.HOSTING_SERVICE_INFO + "}] }"
                + "%-5level"
                + " %logger{36}"
                + " - %msg%n")
    }

    fun createCustomCryptoSettings(tlsConfig: TlsConfig): CryptoSettings {
        // certificates method
        // check that everything is set
        checkNotNull(tlsConfig.publicKeyFile)
        checkNotNull(tlsConfig.privateKeyFile)
        checkNotNull(tlsConfig.caCertFile)
        checkNotNull(tlsConfig.privateKeyPassword)

        return CustomCryptoSettings.fromKeyFile(
            File(tlsConfig.privateKeyFile).path,
            File(tlsConfig.publicKeyFile).path,
            File(tlsConfig.caCertFile).path,
            tlsConfig.privateKeyPassword
        )
    }

    fun localLoggerConfig(consoleLevel: Level?): BuiltConfiguration? {
        val builder = ConfigurationBuilderFactory.newConfigurationBuilder()
        builder.setStatusLevel(Level.ERROR)
        builder.setConfigurationName("LocalLogging")
        val layoutBuilder = builder
            .newLayout("PatternLayout")
            .addAttribute("pattern", CUSTOM_PATTERN)
        val rootLogger = builder.newRootLogger(Level.DEBUG)
        run {

            // create a console appender
            val appenderBuilder = builder
                .newAppender("console_logger", ConsoleAppender.PLUGIN_NAME)
                .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
            appenderBuilder.add(layoutBuilder)
            // only log WARN or worse to console
            appenderBuilder.addComponent(
                builder.newFilter(
                    "ThresholdFilter",
                    Filter.Result.ACCEPT,
                    Filter.Result.DENY
                )
                    .addAttribute("level", consoleLevel)
            )
            builder.add(appenderBuilder)
            rootLogger.add(builder.newAppenderRef(appenderBuilder.name))
        }
        run {
            // quiet down chatty loggers
            CHATTY_LOGGERS.forEach(Consumer { logger: String? ->
                builder.add(
                    builder.newLogger(logger, Level.INFO)
                        .addAttribute("additivity", true)
                )
            })
        }
        builder.add(rootLogger)
        return builder.build()
    }
}