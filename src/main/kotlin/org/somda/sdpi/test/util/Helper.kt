package org.somda.sdpi.test.util

import org.somda.sdc.biceps.model.participant.AbstractMetricValue
import org.somda.sdc.biceps.model.participant.GenerationMode
import org.somda.sdc.biceps.model.participant.LocalizedText
import org.somda.sdc.biceps.model.participant.MeasurementValidity

fun addMetricQualityDemo(value: AbstractMetricValue?) {
    if (value != null) {
        if (value.metricQuality == null) {
            value.metricQuality = AbstractMetricValue.MetricQuality().apply {
                mode = GenerationMode.DEMO
                validity = MeasurementValidity.VLD
            }
        }
    }
}

fun createLocalizedText(text: String?, lang: String? = "en"): LocalizedText {
    val localizedText = LocalizedText()
    localizedText.value = text
    localizedText.lang = lang
    return localizedText
}