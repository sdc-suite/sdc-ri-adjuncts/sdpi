package org.somda.sdpi.test.util.config

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
@SerialName("tls")
data class TlsConfig(
    val disableTls: Boolean = false,
    val publicKeyFile: String? = null,
    val privateKeyFile: String? = null,
    val caCertFile: String? = null,
    val privateKeyPassword: String? = null
)