package org.somda.sdpi.test.util

import org.somda.sdc.biceps.provider.access.LocalMdibAccess
import org.somda.sdc.biceps.provider.access.factory.LocalMdibAccessFactory
import org.somda.sdc.glue.common.MdibXmlIo
import org.somda.sdc.glue.common.factory.ModificationsBuilderFactory
import jakarta.inject.Inject

class MdibLoader @Inject constructor(
    private val modificationsBuilderFactory: ModificationsBuilderFactory,
    private val mdibXmlIo: MdibXmlIo,
    private val mdibAccessFactory: LocalMdibAccessFactory

) {
    fun loadFromResourcePath(resourcePath: String): LocalMdibAccess {
        val mdibAccess = mdibAccessFactory.createLocalMdibAccess()
        val mdibAsStream = MdibLoader::class.java.classLoader.getResourceAsStream(resourcePath)
            ?: throw RuntimeException("Could not load $resourcePath as resource")
        val mdib = mdibXmlIo.readMdib(mdibAsStream)
        val modifications = modificationsBuilderFactory.createModificationsBuilder(mdib).get()
        mdibAccess.writeDescription(modifications)
        return mdibAccess
    }
}