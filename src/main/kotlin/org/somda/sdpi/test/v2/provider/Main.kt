package org.somda.sdpi.test.v2.provider

import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.model.participant.*
import org.somda.sdpi.test.util.AppRunner
import org.somda.sdpi.test.v2.Handles
import java.io.IOException
import java.time.Instant
import java.util.*
import kotlin.concurrent.schedule

fun main(args: Array<String>) = Main().main(args)

fun timedMetricAndAlertUpdates(timer: Timer, provider: Provider, providerSetup: ProviderSetup): TimerTask {
    return timer.schedule(
        0,
        providerSetup.config.updateTimes.generalReportIntervalInMillis.toLong(),
    ) {
        val metricModificationsList = mutableListOf<AbstractMetricState>()
        val now = Instant.now()
        provider.changeNumericMetrics(
            metricModificationsList,
            now,
            listOf(
                Handles.NUMERIC_METRIC_0_CHANNEL_0_VMD_0_MDS_0,
                Handles.NUMERIC_METRIC_0_CHANNEL_0_VMD_1_MDS_0,
                Handles.NUMERIC_METRIC_1_CHANNEL_0_VMD_1_MDS_0,
                Handles.NUMERIC_METRIC_1_CHANNEL_1_VMD_0_MDS_0
            )
        )
        provider.changeStringMetrics(
            metricModificationsList,
            now,
            listOf(
                Handles.STRING_METRIC_0_CHANNEL_0_VMD_0_MDS_0
            )
        )
        provider.changeEnumStringMetrics(
            metricModificationsList,
            now,
            listOf(
                Handles.ENUM_STRING_METRIC_0_CHANNEL_0_VMD_0_MDS_0
            )
        )
        val metricModifications = MdibStateModifications.Metric(metricModificationsList)
        provider.submitStateChanges(metricModifications)

        val alertModificationsList = mutableListOf<AbstractAlertState>()
        provider.changeAlertSignalAndConditionPresences(
            alertModificationsList,
            now,
            listOf(
                Handles.ALERT_SIGNAL_0_MDS_0
            )
        )

        val alertModifications = MdibStateModifications.Alert(alertModificationsList)
        provider.submitStateChanges(alertModifications)
    }
}

fun timedWaveformUpdates(timer: Timer, provider: Provider, providerSetup: ProviderSetup): TimerTask {
    return timer.schedule(
        0,
        providerSetup.config.updateTimes.waveformIntervalInMillis.toLong(),
    ) {
        val rtsaList = mutableListOf<RealTimeSampleArrayMetricState>()
        provider.apply {
            changeWaveforms(
                rtsaList,
                Instant.now(),
                listOf(
                    Handles.RTSA_METRIC_0_CHANNEL_1_VMD_0_MDS_0,
                    Handles.RTSA_METRIC_1_CHANNEL_1_VMD_0_MDS_0,
                    Handles.RTSA_METRIC_2_CHANNEL_1_VMD_0_MDS_0
                )
            )
            val modifications = MdibStateModifications.Waveform(rtsaList)
            submitStateChanges(modifications)
        }
    }
}

fun timedComponentUpdates(timer: Timer, provider: Provider, providerSetup: ProviderSetup): TimerTask {
    return timer.schedule(
        0,
        providerSetup.config.updateTimes.generalReportIntervalInMillis.toLong(),
    ) {
        provider.apply {
            val compStates =  mutableListOf<AbstractDeviceComponentState>()
            changeComponentStates(
                compStates,
                listOf(
                    Handles.CLOCK_MDS_0,
                    Handles.VMD_0_MDS_1,
                    Handles.MDS_1,
                    Handles.BATTERY_0_MDS_0
                )
            )
            val modifications = MdibStateModifications.Component(compStates)
            submitStateChanges(modifications)
        }

        provider.apply {
            val opStates = mutableListOf<AbstractOperationState>()
            changeOperationalStates(
                opStates,
                listOf(Handles.ACTIVATE_0_SCO_MDS_0)
            )
            val modifications = MdibStateModifications.Operation(opStates)
            submitStateChanges(modifications)
        }
    }
}

fun timedDescriptionUpdates(timer: Timer, provider: Provider, providerSetup: ProviderSetup): TimerTask {
    return timer.schedule(
        0,
        providerSetup.config.updateTimes.descriptionUpdateIntervalInMillis.toLong(),
    ) {
        provider.changeDescription()
    }
}

class Main : AppRunner() {
    override fun run() {
        val providerSetup = ProviderSetup(this)
        val provider = Provider(providerSetup)

        // set a location for scopes
        val locationDetail = LocationDetail().apply {
            bed = providerSetup.config.location.bed
            poC = providerSetup.config.location.poc
            facility = providerSetup.config.location.facility
        }

        provider.startAsync().awaitRunning()

        provider.setLocation(locationDetail)

        logger.info { "Sending general state reports every ${providerSetup.config.updateTimes.generalReportIntervalInMillis}ms" }
        logger.info { "Sending waveforms every ${providerSetup.config.updateTimes.waveformIntervalInMillis}ms" }
        logger.info { "Sending description modifications every ${providerSetup.config.updateTimes.descriptionUpdateIntervalInMillis}ms" }

        val timer = Timer()
        val waveformTimer = Timer()
        val threads = listOf(
            timedWaveformUpdates(waveformTimer, provider, providerSetup),
            timedMetricAndAlertUpdates(timer, provider, providerSetup),
            timedComponentUpdates(timer, provider, providerSetup),
            timedDescriptionUpdates(timer, provider, providerSetup)
        )

        // graceful shutdown using sigterm
        Runtime.getRuntime().addShutdownHook(Thread {
            threads.forEach { it.cancel() }
            provider.stopAsync().awaitTerminated()
        })

        try {
            System.`in`.read()
        } catch (e: IOException) {
            // pass and quit
        }
    }
}