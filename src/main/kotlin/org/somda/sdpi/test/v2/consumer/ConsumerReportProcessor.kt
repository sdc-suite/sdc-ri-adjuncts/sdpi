package org.somda.sdpi.test.v2.consumer

import com.google.common.eventbus.Subscribe
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.biceps.common.access.MdibAccessObserver
import org.somda.sdc.biceps.common.event.*
import org.somda.sdc.biceps.model.participant.*

data class CollectedChanges(
    val numericMetricUpdatesCounter: Long = 0L,
    val stringMetricUpdatesCounter: Long = 0L,
    val batteryUpdatesCounter: Long = 0L,
    val clockUpdatesCounter: Long = 0L,
    val mdsVmdUpdatesCounter: Long = 0L,
    val alertSignalUpdatesCounter: Long = 0L,
    val alertConditionUpdatesCounter: Long = 0L,
    val alertSystemUpdatesCounter: Long = 0L,
    val waveformUpdatesCounter: Long = 0L,
    val contextUpdatesCounter: Long = 0L,
    val operationStateUpdatesCounter: Long = 0L,
    val alertConceptDescriptionUpdatesCounter: Long = 0L,
    val alertCauseRemedyUpdatesCounter: Long = 0L,
    val unitUpdatesCounter: Long = 0L,
    val descriptionInsertionCounter: Long = 0L,
    val descriptionDeletionCounter: Long = 0L,
)

/**
 * This class handles incoming reports on the provider.
 *
 */
class ConsumerReportProcessor : MdibAccessObserver {
    var collectedChanges = CollectedChanges()
        get() = field.copy()

    @Subscribe
    fun onMetricChange(modificationMessage: MetricStateModificationMessage) {
        logger.info { "onMetricChange" }
        val numericChanges = modificationMessage.states.values.flatten().filterIsInstance<NumericMetricState>().any()
        if (numericChanges) {
            collectedChanges =
                collectedChanges.copy(numericMetricUpdatesCounter = collectedChanges.numericMetricUpdatesCounter + 1)
        }

        val stringChanges = modificationMessage.states.values.flatten().filterIsInstance<StringMetricState>().any()
        if (stringChanges) {
            collectedChanges =
                collectedChanges.copy(stringMetricUpdatesCounter = collectedChanges.stringMetricUpdatesCounter + 1)
        }
    }

    @Subscribe
    fun onWaveformChange(modificationMessage: WaveformStateModificationMessage) {
        logger.info { "onWaveformChange" }

        if (modificationMessage.states.values.flatten().count() < 3) {
            logger.warn { "Less than 3 waveforms in update" }
            return
        }

        val counter = modificationMessage.states.values.flatten().sumOf {
            if (it.metricValue?.samples?.size?.let { size -> size < 100 } == true) {
                logger.warn { "Less than 100 waveform samples in update" }
                0
            } else {
                it.metricValue?.samples?.size ?: 0
            }
        }
        collectedChanges =
            collectedChanges.copy(waveformUpdatesCounter = collectedChanges.waveformUpdatesCounter + counter)
    }

    @Subscribe
    fun onContextChange(modificationMessage: ContextStateModificationMessage) {
        logger.info { "onContextChange" }
        collectedChanges = collectedChanges.copy(contextUpdatesCounter = collectedChanges.contextUpdatesCounter + 1)
    }

    @Subscribe
    fun onComponentChange(modificationMessage: ComponentStateModificationMessage) {
        logger.info { "onComponentChange" }
        val batteryChanged = modificationMessage.states.values.flatten().filterIsInstance<BatteryState>().any()
        if (batteryChanged) {
            collectedChanges = collectedChanges.copy(batteryUpdatesCounter = collectedChanges.batteryUpdatesCounter + 1)
        }

        val clockChanged = modificationMessage.states.values.flatten().filterIsInstance<ClockState>().any()
        if (clockChanged) {
            collectedChanges = collectedChanges.copy(clockUpdatesCounter = collectedChanges.clockUpdatesCounter + 1)
        }

        val mdsChanged = modificationMessage.states.values.flatten().filterIsInstance<MdsState>().any()
        val vmdChanged = modificationMessage.states.values.flatten().filterIsInstance<VmdState>().any()
        if (mdsChanged || vmdChanged) {
            collectedChanges = collectedChanges.copy(mdsVmdUpdatesCounter = collectedChanges.mdsVmdUpdatesCounter + 1)
        }
    }

    @Subscribe
    fun onAlertChange(modificationMessage: AlertStateModificationMessage) {
        logger.info { "onAlertChange" }
        val conditionUpdate = modificationMessage.states.values.flatten().filterIsInstance<AlertConditionState>().any()
        if (conditionUpdate) {
            collectedChanges =
                collectedChanges.copy(alertConditionUpdatesCounter = collectedChanges.alertConditionUpdatesCounter + 1)
        }

        val signalUpdate = modificationMessage.states.values.flatten().filterIsInstance<AlertSignalState>().any()
        if (signalUpdate) {
            collectedChanges =
                collectedChanges.copy(alertSignalUpdatesCounter = collectedChanges.alertSignalUpdatesCounter + 1)
        }

        val systemUpdate = modificationMessage.states.values.flatten().filterIsInstance<AlertSystemState>().any()
        if (systemUpdate) {
            collectedChanges =
                collectedChanges.copy(alertSystemUpdatesCounter = collectedChanges.alertSystemUpdatesCounter + 1)
        }
    }

    @Subscribe
    fun onOperationChange(modificationMessage: OperationStateModificationMessage) {
        logger.info { "onOperationChange" }
        collectedChanges =
            collectedChanges.copy(operationStateUpdatesCounter = collectedChanges.operationStateUpdatesCounter + 1)
    }

    @Subscribe
    fun descriptionModificationReport(modificationMessage: DescriptionModificationMessage) {
        logger.info { "onDescriptionModification" }
        // todo detect changes in accordance to test specification

        val conditionChanged =
            modificationMessage.updatedEntities.map { it.descriptor }.filterIsInstance<AlertConditionDescriptor>().any()

        if (conditionChanged) {
            collectedChanges = collectedChanges.copy(
                alertConceptDescriptionUpdatesCounter = collectedChanges.alertConceptDescriptionUpdatesCounter + 1,
                alertCauseRemedyUpdatesCounter = collectedChanges.alertCauseRemedyUpdatesCounter + 1
            )
        }

        val metricChanged =
            modificationMessage.updatedEntities.map { it.descriptor }.filterIsInstance<AbstractMetricDescriptor>().any()

        if (metricChanged) {
            collectedChanges =
                collectedChanges.copy(unitUpdatesCounter = collectedChanges.unitUpdatesCounter + 1)
        }

        if (modificationMessage.insertedEntities.isNotEmpty()) {
            collectedChanges =
                collectedChanges.copy(descriptionInsertionCounter = collectedChanges.descriptionInsertionCounter + 1)
        }

        if (modificationMessage.deletedEntities.isNotEmpty()) {
            collectedChanges =
                collectedChanges.copy(descriptionDeletionCounter = collectedChanges.descriptionDeletionCounter + 1)
        }
    }

    private companion object : Logging
}
