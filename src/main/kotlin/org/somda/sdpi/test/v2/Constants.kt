package org.somda.sdpi.test.v2

object Constants {
    const val NAMESPACE_PREFIX = "sdpi"
    const val NAMESPACE = "urn:oid:1.3.6.1.4.1.19376.1.6.2.10.1.1.1"
    const val NAMESPACE_PREFIX_MAPPING_SDPI = "{$NAMESPACE_PREFIX:$NAMESPACE}"
}