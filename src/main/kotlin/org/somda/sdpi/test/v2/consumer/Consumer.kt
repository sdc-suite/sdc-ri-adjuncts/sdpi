package org.somda.sdpi.test.v2.consumer

import com.google.common.eventbus.Subscribe
import com.google.common.util.concurrent.AbstractIdleService
import com.google.common.util.concurrent.SettableFuture
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.biceps.common.CodedValueUtil
import org.somda.sdc.biceps.common.MdibEntity
import org.somda.sdc.biceps.common.access.MdibAccess
import org.somda.sdc.biceps.model.message.*
import org.somda.sdc.biceps.model.participant.*
import org.somda.sdc.common.util.copyTyped
import org.somda.sdc.dpws.DpwsFramework
import org.somda.sdc.dpws.client.Client
import org.somda.sdc.dpws.client.DiscoveredDevice
import org.somda.sdc.dpws.client.DiscoveryObserver
import org.somda.sdc.dpws.client.event.DeviceEnteredMessage
import org.somda.sdc.dpws.client.event.ProbedDeviceFoundMessage
import org.somda.sdc.dpws.service.HostingServiceProxy
import org.somda.sdc.dpws.soap.SoapUtil
import org.somda.sdc.dpws.soap.interception.Interceptor
import org.somda.sdc.dpws.wsdl.WsdlRetriever
import org.somda.sdc.glue.common.ActionConstants
import org.somda.sdc.glue.common.WsdlConstants
import org.somda.sdc.glue.consumer.*
import org.somda.sdpi.test.v2.Handles
import java.io.IOException
import java.math.BigDecimal
import java.net.InetAddress
import java.net.NetworkInterface
import java.time.Duration
import java.time.Instant
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import java.util.stream.Collectors
import kotlin.system.exitProcess


class Consumer(consumerSetup: ConsumerSetup) : AbstractIdleService() {
    private val injector = consumerSetup.injector
    private val config = consumerSetup.config

    private val client = injector.getInstance(Client::class.java)
    private val connector = injector.getInstance(SdcRemoteDevicesConnector::class.java)

    private var epr = config.epr

    private val networkInterface = checkNotNull(NetworkInterface.getByInetAddress(
        InetAddress.getByName(config.address)
    )) { "Could not determine network adapter using ip address ${config.address}" }

    private val dpwsFramework = injector.getInstance(DpwsFramework::class.java).apply {
        setNetworkInterface(networkInterface)
    }

    private val soapUtil = injector.getInstance(SoapUtil::class.java)

    override fun startUp() {
        // provide the name of your network adapter
        dpwsFramework.startAsync().awaitRunning()
        client.startAsync().awaitRunning()
    }

    override fun shutDown() {
        client.stopAsync().awaitTerminated(10, TimeUnit.SECONDS)
        dpwsFramework.stopAsync().awaitTerminated(10, TimeUnit.SECONDS)
    }

    fun runTests() {
        if (epr == null) {
            logger.error { "The config must provide an EPR to perform the tests, but non was found. Abort." }
            exitProcess(1)
        }

        // see if device using the provided epr address is available
        logger.info { "Starting discovery for $epr" }

        val discoveredDeviceFuture = SettableFuture.create<DiscoveredDevice>()
        val obs: DiscoveryObserver = object : DiscoveryObserver {
            @Subscribe
            fun deviceFound(message: ProbedDeviceFoundMessage) {
                val payload = message.payload
                if (matchEpr(payload.eprAddress)) {
                    logger.info { "Found device with epr ${payload.eprAddress}" }
                    Tests.pass(Test.Step.TS_1_B)
                    discoveredDeviceFuture.set(payload)
                } else {
                    logger.info { "Found non-matching device with epr ${payload.eprAddress}" }
                }
            }

            @Subscribe
            fun deviceEntered(message: DeviceEnteredMessage) {
                val payload = message.payload
                if (matchEpr(payload.eprAddress)) {
                    logger.info { "Device with epr ${payload.eprAddress} entered network" }
                    Tests.pass(Test.Step.TS_1_A)
                    discoveredDeviceFuture.set(payload)
                }
            }
        }

        client.registerDiscoveryObserver(obs)

        // filter discovery for SDC devices only
        val discoveryFilterBuilder = SdcDiscoveryFilterBuilder.create()
        client.probe(discoveryFilterBuilder.get())
        val discoveredDevice = try {
            discoveredDeviceFuture.get(MAX_WAIT_SEC, TimeUnit.SECONDS)
        } catch (e: TimeoutException) {
            discoveredDeviceFuture.cancel(true)
            "Couldn't find target with EPR $epr after $MAX_WAIT_SEC seconds".also {
                logger.error(e) { it }
                Tests.fail(Test.Step.TS_1_A, it)
                Tests.fail(Test.Step.TS_1_B, it)
            }
            exitProcess(1)
        } catch (e: Exception) {
            "Couldn't find target with EPR $epr".also {
                logger.error(e) { it }
                Tests.fail(Test.Step.TS_1_A, it)
                Tests.fail(Test.Step.TS_1_B, it)
            }
            exitProcess(1)
        }

        epr = discoveredDevice.eprAddress

        client.unregisterDiscoveryObserver(obs)
        logger.info { "Connecting to $epr@{'${discoveredDevice.xAddrs.joinToString("', '")}'}" }

        val hostingServiceFuture = client.connect(discoveredDevice)
        val hostingServiceProxy = try {
            hostingServiceFuture.get(MAX_WAIT_SEC, TimeUnit.SECONDS).also {
                Tests.pass(Test.Step.TS_2_A)
            }
        } catch (e: TimeoutException) {
            discoveredDeviceFuture.cancel(true)
            "Couldn't connect to EPR $epr after $MAX_WAIT_SEC seconds".also {
                logger.error(e) { it }
                Tests.fail(Test.Step.TS_2_A, it)
            }
            exitProcess(1)
        } catch (e: Exception) {
            "Couldn't connect to EPR $epr".also {
                logger.error(e) { it }
                Tests.fail(Test.Step.TS_2_A, it)
            }
            exitProcess(1)
        }

        // optionally retrieve the wsdl
        logger.info { "Retrieving device WSDL" }
        val wsdlRetriever = injector.getInstance(WsdlRetriever::class.java)
        try {
            val wsdls = wsdlRetriever.retrieveWsdls(hostingServiceProxy)
            logger.debug { "Retrieved WSDLs" }
            if (logger.delegate.isDebugEnabled) {
                for (wsdl in wsdls) {
                    logger.debug { "WSDLs for service ${wsdl.key}: ${wsdl.value}" }
                }
            }
        } catch (e: IOException) {
            logger.error(e) { "Could not retrieve WSDL" }
        }

        logger.info { "Attaching to remote mdib and subscriptions for $epr" }

        val remoteDeviceFuture = connector.connect(
            hostingServiceProxy,
            ConnectConfiguration.create(ConnectConfiguration.ALL_EPISODIC_AND_WAVEFORM_REPORTS)
        )
        val sdcRemoteDevice = try {
            remoteDeviceFuture.get(MAX_WAIT_SEC, TimeUnit.SECONDS)
        } catch (e: TimeoutException) {
            remoteDeviceFuture.cancel(true)
            "Couldn't attach to remote mdib and subscriptions for $epr after $MAX_WAIT_SEC seconds".also {
                logger.error(e) { it }
            }
            exitProcess(1)
        } catch (e: Exception) {
            "Couldn't attach to remote mdib and subscriptions for $epr".also {
                logger.error(e) { it }
            }
            exitProcess(1)
        }

        // attach report listener
        val reportProcessor = ConsumerReportProcessor()
        sdcRemoteDevice.mdibAccessObservable.registerObserver(reportProcessor)

        runCatching {
            requestMdib(hostingServiceProxy)
        }
        runCatching {
            requestContextStates(hostingServiceProxy)
        }

        runCatching {
            testRenew(hostingServiceProxy)
        }

        runCatching {
            testOperationInvocations(sdcRemoteDevice)
        }

        // wait for updates to arrive
        Thread.sleep(40_000)

        val expectedStateUpdatesCount = 5

        if (reportProcessor.collectedChanges.numericMetricUpdatesCounter >= expectedStateUpdatesCount) {
            Tests.pass(Test.Step.TS_4_A)
        } else {
            Tests.fail(
                Test.Step.TS_4_A,
                "Received only ${reportProcessor.collectedChanges.numericMetricUpdatesCounter} numeric metric updates"
            )
        }

        if (reportProcessor.collectedChanges.numericMetricUpdatesCounter >= expectedStateUpdatesCount) {
            Tests.pass(Test.Step.TS_4_B)
        } else {
            Tests.fail(
                Test.Step.TS_4_B,
                "Received only ${reportProcessor.collectedChanges.numericMetricUpdatesCounter} string metric updates"
            )
        }

        if (reportProcessor.collectedChanges.alertConditionUpdatesCounter >= expectedStateUpdatesCount) {
            Tests.pass(Test.Step.TS_4_C)
        } else {
            Tests.fail(
                Test.Step.TS_4_C,
                "Received only ${reportProcessor.collectedChanges.alertConditionUpdatesCounter} alert conditions"
            )
        }

        if (reportProcessor.collectedChanges.alertSignalUpdatesCounter >= expectedStateUpdatesCount) {
            Tests.pass(Test.Step.TS_4_D)
        } else {
            Tests.fail(
                Test.Step.TS_4_D,
                "Received only ${reportProcessor.collectedChanges.alertSignalUpdatesCounter} alert signals"
            )
        }

        if (reportProcessor.collectedChanges.alertSignalUpdatesCounter >= expectedStateUpdatesCount) {
            Tests.pass(Test.Step.TS_4_D)
        } else {
            Tests.fail(
                Test.Step.TS_4_D,
                "Received only ${reportProcessor.collectedChanges.alertSignalUpdatesCounter} alert signals"
            )
        }

        if (reportProcessor.collectedChanges.alertSystemUpdatesCounter >= expectedStateUpdatesCount) {
            Tests.pass(Test.Step.TS_4_E)
        } else {
            Tests.fail(
                Test.Step.TS_4_E,
                "Received only ${reportProcessor.collectedChanges.alertSystemUpdatesCounter} alert system states"
            )
        }

        val expectedWaveformUpdatesCounter = 3 * 10 * 100 * 30
        if (reportProcessor.collectedChanges.waveformUpdatesCounter >= expectedWaveformUpdatesCounter) {
            Tests.pass(Test.Step.TS_4_F)
        } else {
            Tests.fail(
                Test.Step.TS_4_F,
                "Received only ${reportProcessor.collectedChanges.waveformUpdatesCounter} waveform samples"
            )
        }

        val clockUpdates = reportProcessor.collectedChanges.clockUpdatesCounter >= expectedStateUpdatesCount
        val batteryUpdates = reportProcessor.collectedChanges.batteryUpdatesCounter >= expectedStateUpdatesCount
        val mdsVmdUpdates = reportProcessor.collectedChanges.mdsVmdUpdatesCounter >= expectedStateUpdatesCount

        if ((clockUpdates || batteryUpdates) && mdsVmdUpdates) {
            Tests.pass(Test.Step.TS_4_G)
        } else {
            Tests.fail(
                Test.Step.TS_4_G,
                "Received either too little clock (${reportProcessor.collectedChanges.clockUpdatesCounter}), battery (${reportProcessor.collectedChanges.batteryUpdatesCounter}), MDS or VMD (${reportProcessor.collectedChanges.mdsVmdUpdatesCounter}) updates"
            )
        }

        if (reportProcessor.collectedChanges.operationStateUpdatesCounter >= expectedStateUpdatesCount) {
            Tests.pass(Test.Step.TS_4_H)
        } else {
            Tests.fail(
                Test.Step.TS_4_H,
                "Received only ${reportProcessor.collectedChanges.operationStateUpdatesCounter} operational states"
            )
        }

        val expectedDescrUpdatesCount = 3

        val alertConceptDescrUpdates =
            reportProcessor.collectedChanges.alertConceptDescriptionUpdatesCounter >= expectedDescrUpdatesCount
        val alertCauseRemedyUpdates =
            reportProcessor.collectedChanges.alertCauseRemedyUpdatesCounter >= expectedDescrUpdatesCount
        val unitOfMeasureUpdates = reportProcessor.collectedChanges.unitUpdatesCounter >= expectedDescrUpdatesCount

        if (alertConceptDescrUpdates && alertCauseRemedyUpdates && unitOfMeasureUpdates) {
            Tests.pass(Test.Step.TS_5_A)
        } else {
            Tests.fail(
                Test.Step.TS_5_A,
                "Received either too little alert concept description updates (${reportProcessor.collectedChanges.alertConceptDescriptionUpdatesCounter}), alert cause remedy updates (${reportProcessor.collectedChanges.alertCauseRemedyUpdatesCounter}), or unit of measure updates (${reportProcessor.collectedChanges.unitUpdatesCounter})"
            )
        }

        val descrInsertionUpdates =
            reportProcessor.collectedChanges.descriptionInsertionCounter >= expectedDescrUpdatesCount
        val descrDeletionUpdates =
            reportProcessor.collectedChanges.descriptionDeletionCounter >= expectedDescrUpdatesCount

        if (descrInsertionUpdates && descrDeletionUpdates) {
            Tests.pass(Test.Step.TS_5_B)
        } else {
            Tests.fail(
                Test.Step.TS_5_B,
                "Received either too little insertions (${reportProcessor.collectedChanges.descriptionInsertionCounter}) or deletions (${reportProcessor.collectedChanges.descriptionDeletionCounter})"
            )
        }


        // todo taken from sequence v1, enable and adapt to sequence v2
//        // has patient
//        val numPatientContexts = contextStates.stream()
//            .filter { x: AbstractContextState ->
//                PatientContextState::class.java.isAssignableFrom(
//                    x.javaClass
//                )
//            }.count()
//        tests[5] = numPatientContexts >= 1
//        // has location context
//        val numLocationContexts = contextStates.stream()
//            .filter { x: AbstractContextState ->
//                LocationContextState::class.java.isAssignableFrom(
//                    x.javaClass
//                )
//            }.count()
//        tests[6] = numLocationContexts >= 1
//
//        // wait for incoming reports
//        Thread.sleep(REPORT_TIMEOUT)
//
//        // expected number of reports given 5 second interval
//        val minNumberReports = (REPORT_TIMEOUT / Duration.ofSeconds(5).toMillis()) - 1
//
//        // verify the number of reports for the expected metrics is at least five during the timeout
//        val metricChangesOk: Unit =
//            reportProcessor.getMetricChanges().values().stream().anyMatch { changes -> changes >= minNumberReports }
//        tests[7] = metricChangesOk
//        if (!metricChangesOk) {
//            LOG.info("Did not see enough metric changes, map: {}", reportProcessor.getMetricChanges())
//        }
//        val conditionChangesOk: Unit =
//            reportProcessor.getConditionChanges().values().stream().anyMatch { changes -> changes >= minNumberReports }
//        tests[8] = conditionChangesOk
//        if (!conditionChangesOk) {
//            LOG.info("Did not see enough alert changes, map: {}", reportProcessor.getConditionChanges())
//        }
//
//        // invoke all target operations
//        val setServiceAccess = sdcRemoteDevice.setServiceAccess
//        var operationFailed = false
//        // find set string operation handle
//        val setStringHandles = getHandleForCodedValue(
//            sdcRemoteDevice.mdibAccess,
//            Constants.HANDLE_SET_STRING_CODE,
//            SetStringOperationDescriptor::class.java
//        )
//        LOG.info("Found {} handles matching code {}", setStringHandles.size, Constants.HANDLE_SET_STRING_CODE)
//        var setStringAnyPass = false
//        for (handle in setStringHandles) {
//            try {
//                LOG.info("Found handle {} and hardcoded handle was {}", handle, Constants.HANDLE_SET_STRING)
//                invokeSetString(setServiceAccess, handle, "SDCri was here")
//                setStringAnyPass = true
//            } catch (e: ExecutionException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_STRING, e)
//            } catch (e: TimeoutException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_STRING, e)
//            } catch (e: InterruptedException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_STRING, e)
//            } catch (e: IndexOutOfBoundsException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_STRING, e)
//            }
//        }
//        if (!setStringAnyPass) {
//            operationFailed = true
//        }
//        // find set string operation handle for enum
//        val paths = getPathToCodedValue(
//            sdcRemoteDevice.mdibAccess,
//            Constants.HANDLE_SET_STRING_ENUM_CODE,
//            SetStringOperationDescriptor::class.java
//        )
//        LOG.info("Found {} paths matching code {}", paths.size, Constants.HANDLE_SET_STRING_ENUM_CODE)
//        var setEnumStringAnyPass = false
//        for (path in paths) {
//            try {
//                val handle = path[path.size - 1]
//                LOG.info("Found handle {} and hardcoded handle was {}", handle, Constants.HANDLE_SET_STRING_ENUM)
//                // get an allowed value
//                val opDesc = sdcRemoteDevice.mdibAccess.getDescriptor(handle).get() as SetStringOperationDescriptor
//                val desc = sdcRemoteDevice.mdibAccess.getDescriptor(opDesc.operationTarget)
//                    .get() as EnumStringMetricDescriptor
//                val value = desc.allowedValue[0].value
//                invokeSetString(setServiceAccess, handle, value)
//                setEnumStringAnyPass = true
//            } catch (e: ExecutionException) {
//                operationFailed = true
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_STRING_ENUM, e)
//            } catch (e: TimeoutException) {
//                operationFailed = true
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_STRING_ENUM, e)
//            } catch (e: InterruptedException) {
//                operationFailed = true
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_STRING_ENUM, e)
//            } catch (e: IndexOutOfBoundsException) {
//                operationFailed = true
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_STRING_ENUM, e)
//            }
//        }
//        if (!setEnumStringAnyPass) {
//            operationFailed = true
//        }
//        // find set value operation handle
//        val setValueHandles = getHandleForCodedValue(
//            sdcRemoteDevice.mdibAccess,
//            Constants.HANDLE_SET_VALUE_CODE,
//            SetValueOperationDescriptor::class.java
//        )
//        LOG.info("Found {} setValueHandles matching code {}", setValueHandles.size, Constants.HANDLE_SET_VALUE_CODE)
//        var setValueAnyPass = false
//        for (handle in setValueHandles) {
//            try {
//                LOG.info("Found handle {} and hardcoded handle was {}", handle, Constants.HANDLE_SET_VALUE)
//                invokeSetValue(setServiceAccess, handle, BigDecimal.valueOf(20))
//                setValueAnyPass = true
//            } catch (e: ExecutionException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_VALUE, e)
//            } catch (e: TimeoutException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_VALUE, e)
//            } catch (e: InterruptedException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_VALUE, e)
//            } catch (e: IndexOutOfBoundsException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_SET_VALUE, e)
//            }
//        }
//        if (!setValueAnyPass) {
//            operationFailed = true
//        }
//        // find activate operation handle
//        val activatePaths = getPathToCodedValue(
//            sdcRemoteDevice.mdibAccess,
//            Constants.HANDLE_ACTIVATE_CODE,
//            ActivateOperationDescriptor::class.java
//        )
//        LOG.info("Found {} handles matching code {}", activatePaths.size, Constants.HANDLE_ACTIVATE_CODE)
//        var activateAnyPass = false
//        for (path in activatePaths) {
//            try {
//                val handle = path[path.size - 1]
//                LOG.info("Found handle {} and hardcoded handle was {}", handle, Constants.HANDLE_ACTIVATE)
//                invokeActivate(setServiceAccess, handle, emptyList<String>())
//                activateAnyPass = true
//            } catch (e: ExecutionException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_ACTIVATE, e)
//            } catch (e: TimeoutException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_ACTIVATE, e)
//            } catch (e: InterruptedException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_ACTIVATE, e)
//            } catch (e: IndexOutOfBoundsException) {
//                LOG.error("Could not invoke {}", Constants.HANDLE_ACTIVATE, e)
//            }
//        }
//        if (!activateAnyPass) {
//            operationFailed = true
//        }
//        tests[9] = !operationFailed


        logger.info { "Done, quitting" }
        try {
            sdcRemoteDevice.mdibAccessObservable.unregisterObserver(reportProcessor)
            connector.disconnect(epr!!).get(10, TimeUnit.SECONDS)
            stopAsync().awaitTerminated(10, TimeUnit.SECONDS)
            Tests.pass(Test.Step.TS_7)
        } catch (e: Exception) {
            "Graceful shutdown failed".also {
                logger.error(e) { it }
                Tests.fail(Test.Step.TS_7, it)
            }
        }
    }

    private fun testOperationInvocations(sdcRemoteDevice: SdcRemoteDevice) {
        runCatching {
            invokeSetContextState(sdcRemoteDevice.mdibAccess, sdcRemoteDevice.setServiceAccess)
        }
        runCatching {
            invokeSetValue(sdcRemoteDevice.mdibAccess, sdcRemoteDevice.setServiceAccess)
        }
        runCatching {
            invokeSetString(sdcRemoteDevice.mdibAccess, sdcRemoteDevice.setServiceAccess)
        }
        runCatching {
            invokeSetMetricStates(sdcRemoteDevice.mdibAccess, sdcRemoteDevice.setServiceAccess)
        }
    }

    private fun invokeSetMetricStates(
        mdibAccess: MdibAccess,
        setServiceAccess: SetServiceAccess
    ) {
        val operationDescriptor = try {
            mdibAccess.findEntitiesByType(SetMetricStateOperationDescriptor::class.java).first {
                it.descriptor.type?.code == "67108890"
            }.descriptor as SetMetricStateOperationDescriptor
        } catch (e: Exception) {
            Tests.fail(
                Test.Step.TS_6_E,
                "No SetMetricState operation found for code 67108890@urn:oid:1.3.111.2.11073.10101"
            )
            return
        }

        val component = mdibAccess.getEntity(operationDescriptor.operationTarget).orElseThrow {
            "Operation target not found: ${operationDescriptor.operationTarget}".let {
                Tests.fail(
                    Test.Step.TS_6_E,
                    it
                )
                Exception(it)
            }
        }

        val states = component.children.mapNotNull { mdibAccess.getEntity(it).orElse(null) }
            .filter { it.descriptor is NumericMetricDescriptor }
            .take(2)
            .map { entity ->
                (entity.states.first() as NumericMetricState).copyTyped().apply {
                    stateVersion = null
                    descriptorVersion = null
                    metricValue?.let {
                        it.value = it.value?.inc() ?: BigDecimal.ONE
                    } ?: NumericMetricValue().apply {
                        value = BigDecimal.ZERO
                        determinationTime = Instant.now()
                    }
                }
            }

        val setMetricState = SetMetricState().apply {
            operationHandleRef = operationDescriptor.handle
            proposedMetricState = states
        }

        val setFuture = setServiceAccess
            .invoke(setMetricState, SetMetricStateResponse::class.java)
        val setMetricStateResponse = setFuture.get(MAX_WAIT_SEC, TimeUnit.SECONDS)
        val reportParts = setMetricStateResponse.waitForFinalReport(Duration.ofSeconds(MAX_WAIT_SEC))

        if (reportParts.last().key.invocationInfo.invocationState == InvocationState.FAIL) {
            Tests.fail(
                Test.Step.TS_6_E,
                "Invocation of SetMetricState failed. Error text: " +
                        (setMetricStateResponse.response.invocationInfo.invocationErrorMessage.firstOrNull()?.value
                            ?: "None")
            )
            return
        }

        Tests.pass(Test.Step.TS_6_E)
    }

    private fun invokeSetString(
        mdibAccess: MdibAccess,
        setServiceAccess: SetServiceAccess
    ) {
        val operationDescriptor = try {
            mdibAccess.findEntitiesByType(SetStringOperationDescriptor::class.java).first {
                it.descriptor.type?.code == "67108889"
            }.descriptor as SetStringOperationDescriptor
        } catch (e: Exception) {
            Tests.fail(
                Test.Step.TS_6_D,
                "No SetString operation found for code 67108889@urn:oid:1.3.111.2.11073.10101"
            )
            return
        }

        val enumMetric =
            mdibAccess.getState(operationDescriptor.operationTarget, EnumStringMetricState::class.java).orElseThrow {
                "No suitable state found for ${operationDescriptor.operationTarget} in order to invoke SetString".let {
                    Tests.fail(
                        Test.Step.TS_6_D,
                        it
                    )
                    Exception(it)
                }
            }

        val requestedValue = if (enumMetric.metricValue?.value == null || enumMetric.metricValue?.value == "ON") {
            "OFF"
        } else {
            "ON"
        }

        val setString = SetString().apply {
            operationHandleRef = operationDescriptor.handle
            requestedStringValue = requestedValue
        }

        val setFuture = setServiceAccess
            .invoke(setString, SetStringResponse::class.java)
        val setStringResponse = setFuture.get(MAX_WAIT_SEC, TimeUnit.SECONDS)
        val reportParts = setStringResponse.waitForFinalReport(Duration.ofSeconds(MAX_WAIT_SEC))

        if (reportParts.last().key.invocationInfo.invocationState == InvocationState.FAIL) {
            Tests.fail(
                Test.Step.TS_6_D,
                "Invocation of SetString failed. Error text: " +
                        (setStringResponse.response.invocationInfo.invocationErrorMessage.firstOrNull()?.value
                            ?: "None")
            )
            return
        }

        Tests.pass(Test.Step.TS_6_D)
    }

    private fun invokeSetValue(
        mdibAccess: MdibAccess,
        setServiceAccess: SetServiceAccess
    ) {
        val operationDescriptor = try {
            val setValues = mdibAccess.findEntitiesByType(SetValueOperationDescriptor::class.java)
            setValues.first {
                it.descriptor.type?.code == "67108888"
            }.descriptor as SetValueOperationDescriptor
        } catch (e: Exception) {
            Tests.fail(
                Test.Step.TS_6_C,
                "No SetValue operation found for code 67108888@urn:oid:1.3.111.2.11073.10101"
            )
            return
        }

        val setValue = SetValue().apply {
            operationHandleRef = operationDescriptor.handle
            requestedNumericValue = 50.3.toBigDecimal()
        }

        val setFuture = setServiceAccess
            .invoke(setValue, SetValueResponse::class.java)
        val setValueResponse = setFuture.get(MAX_WAIT_SEC, TimeUnit.SECONDS)
        val reportParts = setValueResponse.waitForFinalReport(Duration.ofSeconds(MAX_WAIT_SEC))

        if (reportParts.last().key.invocationInfo.invocationState == InvocationState.FAIL) {
            Tests.fail(
                Test.Step.TS_6_C,
                "Invocation of SetValue failed. Error text: " +
                        (setValueResponse.response.invocationInfo.invocationErrorMessage.firstOrNull()?.value
                            ?: "None")
            )
            return
        }

        Tests.pass(Test.Step.TS_6_C)
    }

    private fun invokeSetContextState(
        mdibAccess: MdibAccess,
        setServiceAccess: SetServiceAccess
    ) {
        val operationDescriptor = try {
            val ops = mdibAccess.findEntitiesByType(SetContextStateOperationDescriptor::class.java)
            ops.first {
                it.descriptor.type?.code == "67108887"
            }.descriptor as SetContextStateOperationDescriptor
        } catch (e: Exception) {
            Tests.fail(
                Test.Step.TS_6_B,
                "No SetPatientContextState operation found for code 67108887@urn:oid:1.3.111.2.11073.10101"
            )
            return
        }

        val setContextState = SetContextState().apply {
            operationHandleRef = operationDescriptor.handle
            proposedContextState = listOf(PatientContextState().apply {
                descriptorHandle = operationDescriptor.operationTarget
                handle = Handles.PATIENT_CONTEXT_MDS_0
                coreData = PatientDemographicsCoreData().apply {
                    sex = Sex.M
                    givenname = "Max"
                    familyname = "Power"
                }
                contextAssociation = ContextAssociation.ASSOC
                identification = listOf(InstanceIdentifier().apply {
                    rootName = "urn:sdpi:patients"
                    extensionName = "MaxPower123"
                })
                validator = listOf(InstanceIdentifier().apply {
                    rootName = "urn.sdpi:validators"
                    extensionName = "GenericRemoteUser"
                    type = CodedValue().apply {
                        symbolicCodeName = "MDC_CTX_XC_VALIDATED"
                        code = "532644"
                    }
                })
            })
        }

        val setFuture = setServiceAccess
            .invoke(setContextState, SetContextStateResponse::class.java)
        val setContextStateResponse = setFuture.get(MAX_WAIT_SEC, TimeUnit.SECONDS)
        val reportParts = setContextStateResponse.waitForFinalReport(Duration.ofSeconds(MAX_WAIT_SEC))

        if (reportParts.last().key.invocationInfo.invocationState == InvocationState.FAIL) {
            Tests.fail(
                Test.Step.TS_6_B,
                "Invocation of SetPatientContextState failed. Error text: " +
                        (setContextStateResponse.response.invocationInfo.invocationErrorMessage.firstOrNull()?.value
                            ?: "None")
            )
            return
        }

        Tests.pass(Test.Step.TS_6_B)
    }

    fun matchEpr(foundEpr: String) = epr?.let { foundEpr.contains(it) } ?: false

    private fun testRenew(hostingServiceProxy: HostingServiceProxy) = Thread {
        val stateEventService = hostingServiceProxy.hostedServices.values
            .firstOrNull { it.type.types.contains(WsdlConstants.PORT_TYPE_STATE_EVENT_QNAME) }
        if (stateEventService == null) {
            "Couldn't find BICEPS STATE EVENT service, which is needed to renew".also {
                logger.error { it }
                Tests.fail(Test.Step.TS_2_B, it)
            }
            return@Thread
        }

        val subscribeResult = try {
            val subscribeResult = stateEventService.eventSinkAccess.subscribe(
                listOf(ActionConstants.ACTION_EPISODIC_ALERT_REPORT),
                Duration.ofHours(1),
                object : Interceptor {}
            ).get()

            if (subscribeResult.grantedExpires.toSeconds() > 15) {
                "Subscription granted was greater than 15 seconds: ${subscribeResult.grantedExpires.toSeconds()}".also {
                    logger.error { it }
                    Tests.fail(Test.Step.TS_2_B, it)
                }
                return@Thread
            }

            subscribeResult
        } catch (e: Exception) {
            "Could not subscribe to BICEPS STATE EVENT service: ${e.message}".also {
                logger.error { it }
                Tests.fail(Test.Step.TS_2_B, it)
            }
            return@Thread
        }

        try {
            stateEventService.eventSinkAccess.renew(subscribeResult.subscriptionId, Duration.ofHours(1)).get()
        } catch (e: Exception) {
            "Could not renew subscription: ${subscribeResult.subscriptionId}".also {
                logger.error { it }
                Tests.fail(Test.Step.TS_2_B, it)
            }
            return@Thread
        }

        Tests.pass(Test.Step.TS_2_B)
    }.start()

    private fun requestMdib(hostingServiceProxy: HostingServiceProxy) {
        val getService = hostingServiceProxy.hostedServices.values
            .firstOrNull { it.type.types.contains(WsdlConstants.PORT_TYPE_GET_QNAME) }
        if (getService == null) {
            "Couldn't find BICEPS GET service".also {
                logger.error { it }
                Tests.fail(Test.Step.TS_3_A, it)
            }
            return
        }

        val response = getService.requestResponseClient.sendRequestResponse(
            soapUtil.createMessage(ActionConstants.ACTION_GET_MDIB, GetMdib())
        )
        val getMdibResponse = soapUtil.getBody(
            response,
            GetMdibResponse::class.java
        )

        if (getMdibResponse.isEmpty) {
            "GetMdib did not respond with a correct GetMdibResponse".also {
                logger.error { it }
                Tests.fail(Test.Step.TS_3_A, it)
            }
            return
        }

        Tests.pass(Test.Step.TS_3_A)
    }

    private fun requestContextStates(hostingServiceProxy: HostingServiceProxy) {
        val contextService = hostingServiceProxy.hostedServices.values
            .firstOrNull { it.type.types.contains(WsdlConstants.PORT_TYPE_CONTEXT_QNAME) }
        if (contextService == null) {
            "Couldn't find BICEPS CONTEXT service".also {
                logger.error { it }
                Tests.fail(Test.Step.TS_3_B, it)
            }
            return
        }

        val response = contextService.requestResponseClient.sendRequestResponse(
            soapUtil.createMessage(ActionConstants.ACTION_GET_CONTEXT_STATES, GetContextStates())
        )
        val getContextStatesResponse = soapUtil.getBody(
            response,
            GetContextStatesResponse::class.java
        )

        if (getContextStatesResponse.isEmpty) {
            "GetContextStates did not respond with a correct GetContextStatesResponse".also {
                logger.error { it }
                Tests.fail(Test.Step.TS_3_B, it)
            }
            return
        }

        Tests.pass(Test.Step.TS_3_B)
    }

    companion object : Logging {
        private val MAX_WAIT = Duration.ofSeconds(11)
        private val MAX_WAIT_SEC = MAX_WAIT.seconds

        /**
         * Synchronously invokes an ActivateOperation on a given SetService using the provided handle and arguments
         *
         * @param setServiceAccess SetService to call Activate on
         * @param handle           operation handle
         * @param args             activate arguments
         * @return InvocationState of final OperationInvokedReport
         * @throws ExecutionException   if retrieving the final OperationInvokedReport is aborted
         * @throws InterruptedException if retrieving the final OperationInvokedReport is interrupted
         * @throws TimeoutException     if retrieving the final OperationInvokedReport times out
         */
        @Throws(ExecutionException::class, InterruptedException::class, TimeoutException::class)
        fun invokeActivate(setServiceAccess: SetServiceAccess, handle: String?, args: List<String?>): InvocationState {
            logger.info { "Invoking Activate for handle $handle with arguments ${args.joinToString(", ")}" }
            val activate = Activate()
            val argumentList = args.stream().map { x: String? ->
                val arg = Activate.Argument()
                arg.argValue = x
                arg
            }.collect(Collectors.toList())
            activate.argument = argumentList
            activate.operationHandleRef = handle
            val activateFuture = setServiceAccess
                .invoke(activate, ActivateResponse::class.java)
            val activateResponse = activateFuture[MAX_WAIT_SEC, TimeUnit.SECONDS]
            if (InvocationState.FAIL == activateResponse.response.invocationInfo.invocationState) {
                val err = "Activate operation execution invocation state failed"
                throw ExecutionException(err, InterruptedException(err))
            }
            val reportParts = activateResponse.waitForFinalReport(Duration.ofSeconds(5))

            // return the final reports invocation state
            return if (!reportParts.isEmpty()) {
                reportParts[reportParts.size - 1].key.invocationInfo.invocationState
            } else {
                throw InterruptedException("No report parts received, help.")
            }
        }

        /**
         * Synchronously invokes a SetValue on a given SetService using the provided handle and argument
         *
         * @param setServiceAccess SetService to call Activate on
         * @param handle           operation handle
         * @param value            desired value of operation target
         * @return InvocationState of final OperationInvokedReport
         * @throws ExecutionException   if retrieving the final OperationInvokedReport is aborted
         * @throws InterruptedException if retrieving the final OperationInvokedReport is interrupted
         * @throws TimeoutException     if retrieving the final OperationInvokedReport times out
         */
        @Throws(ExecutionException::class, InterruptedException::class, TimeoutException::class)
        fun invokeSetValue(setServiceAccess: SetServiceAccess, handle: String?, value: BigDecimal?): InvocationState {
            logger.info { "Invoking SetValue for handle $handle with value $value" }
            val setValue = SetValue()
            setValue.operationHandleRef = handle
            setValue.requestedNumericValue = value
            val setValueFuture = setServiceAccess
                .invoke(setValue, SetValueResponse::class.java)
            val setValueResponse = setValueFuture[MAX_WAIT_SEC, TimeUnit.SECONDS]
            if (InvocationState.FAIL == setValueResponse.response.invocationInfo.invocationState) {
                val err = "SetValue operation execution invocation state failed"
                throw ExecutionException(err, InterruptedException(err))
            }
            val reportParts = setValueResponse.waitForFinalReport(Duration.ofSeconds(5))

            // return the final reports invocation state
            return if (!reportParts.isEmpty()) {
                reportParts[reportParts.size - 1].key.invocationInfo.invocationState
            } else {
                throw InterruptedException("No report parts received, help.")
            }
        }

        /**
         * Synchronously invokes a SetString on a given SetService using the provided handle and argument
         *
         * @param setServiceAccess SetService to call Activate on
         * @param handle           operation handle
         * @param value            desired value of operation target
         * @return InvocationState of final OperationInvokedReport
         * @throws ExecutionException   if retrieving the final OperationInvokedReport is aborted
         * @throws InterruptedException if retrieving the final OperationInvokedReport is interrupted
         * @throws TimeoutException     if retrieving the final OperationInvokedReport times out
         */
        @Throws(ExecutionException::class, InterruptedException::class, TimeoutException::class)
        fun invokeSetString(setServiceAccess: SetServiceAccess, handle: String?, value: String?): InvocationState {
            logger.info { "Invoking SetString for handle $handle with value $value" }
            val setString = SetString()
            setString.operationHandleRef = handle
            setString.requestedStringValue = value
            val setStringFuture = setServiceAccess
                .invoke(setString, SetStringResponse::class.java)
            val setStringResponse = setStringFuture[MAX_WAIT_SEC, TimeUnit.SECONDS]
            if (InvocationState.FAIL == setStringResponse.response.invocationInfo.invocationState) {
                val err = "SetString operation execution invocation state failed"
                throw ExecutionException(err, InterruptedException(err))
            }
            val reportParts = setStringResponse.waitForFinalReport(Duration.ofSeconds(5))

            // return the final reports invocation state
            return if (!reportParts.isEmpty()) {
                reportParts[reportParts.size - 1].key.invocationInfo.invocationState
            } else {
                throw InterruptedException("No report parts received, help.")
            }
        }

        fun getHandleForCodedValue(
            mdib: MdibAccess,
            value: CodedValue?,
            type: Class<out AbstractDescriptor?>?
        ): List<String> {
            // find all applicable entities
            val entities = mdib.findEntitiesByType(type)

            // filter by coded value
            return entities.stream().filter { x: MdibEntity ->
                CodedValueUtil.isEqual(
                    x.descriptor.type, value
                )
            }.map { obj: MdibEntity -> obj.handle }.collect(Collectors.toList())
        }

        fun getPathToCodedValue(
            mdib: MdibAccess,
            value: CodedValue?,
            type: Class<out AbstractDescriptor?>?
        ): List<List<String>> {

            // find all applicable entities
            val entities = mdib.findEntitiesByType(type)

            // filter by coded value
            val applicableEntities = entities.stream().filter { x: MdibEntity ->
                CodedValueUtil.isEqual(
                    x.descriptor.type, value
                )
            }.collect(Collectors.toList())
            return applicableEntities.stream().map { entity: MdibEntity? ->
                var currentEntity = entity
                val handles = ArrayList<String>()
                while (currentEntity != null) {
                    val handle = currentEntity.handle
                    handles.add(0, handle)
                    // get parent entity
                    currentEntity = if (currentEntity.parent.isPresent) {
                        mdib.getEntity(currentEntity.parent.get()).orElse(null)
                    } else {
                        // exit
                        null
                    }
                }
                handles
            }.collect(Collectors.toList())
        }
    }
}