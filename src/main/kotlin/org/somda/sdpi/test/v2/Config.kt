package org.somda.sdpi.test.v2

import org.somda.sdpi.test.util.config.TlsConfig
import org.somda.sdpi.test.v2.provider.ProviderConfig


data class Config (
    val tlsConfig: TlsConfig,
    val providerConfig: ProviderConfig
)