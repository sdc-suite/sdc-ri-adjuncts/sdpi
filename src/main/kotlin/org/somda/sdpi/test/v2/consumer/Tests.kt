package org.somda.sdpi.test.v2.consumer

data class Test(
    val step: Step,
    val result: Result = Result.NOT_EXECUTED,
    val message: String = ""
) {
    enum class Result {
        PASSED,
        FAILED,
        NOT_EXECUTED;

        override fun toString() = name.replace('_', ' ').lowercase()
    }

    // test steps are specified here: https://confluence.hl7.org/pages/viewpage.action?pageId=144998177
    enum class Step {
        TS_1_A,
        TS_1_B,
        TS_2_A,
        TS_2_B,
        TS_3_A,
        TS_3_B,
        TS_4_A,
        TS_4_B,
        TS_4_C,
        TS_4_D,
        TS_4_E,
        TS_4_F,
        TS_4_G,
        TS_4_H,
        TS_5_A,
        TS_5_B,
        TS_6_A,
        TS_6_B,
        TS_6_C,
        TS_6_D,
        TS_6_E,
        TS_7;

        fun passed() = Test(this, Result.PASSED)
        fun failed(msg: String) = Test(this, Result.FAILED, msg)

        override fun toString() = tagRegex.matchEntire(name)!!.let {
            when (val step = it.groups[LABEL_STEP_WITH_SUBSTEP]) {
                null -> it.groups[LABEL_STEP_ONLY]!!.value
                else -> step.value + it.groups[LABEL_SUB_STEP]!!.value.lowercase()
            }
        }

        companion object {
            private const val LABEL_STEP_WITH_SUBSTEP = "stepwsubstep"
            private const val LABEL_SUB_STEP = "substep"
            private const val LABEL_STEP_ONLY = "steponly"
            private val tagRegex = """^TS_((?<$LABEL_STEP_WITH_SUBSTEP>.+)_(?<$LABEL_SUB_STEP>.+)|(?<$LABEL_STEP_ONLY>.+))$""".toRegex()
        }
    }
}

object Tests {
    private val testMap = Test.Step.entries.associateWith { Test(it) }.toMutableMap()

    fun fail(step: Test.Step, message: String) {
        testMap[step] = step.failed(message)
    }

    fun pass(step: Test.Step) {
        testMap[step] = step.passed()
    }

    fun toList(): List<Test> = testMap.values.toMutableList().apply { sortBy { it.step } }
}