package org.somda.sdpi.test.v2.provider

import kotlinx.serialization.SerialName
import org.somda.sdpi.test.util.config.TlsConfig

@kotlinx.serialization.Serializable
@SerialName("provider")
data class ProviderConfig(
    val epr: String? = null,
    val address: String? = null,
    val writeCommlog: Boolean = false,
    val tls: TlsConfig = TlsConfig(),
    val location: LocationConfig = LocationConfig(),
    val updateTimes: UpdateTimesConfig = UpdateTimesConfig()
)

@kotlinx.serialization.Serializable
@SerialName("location")
data class LocationConfig(
    val facility: String? = null,
    val poc: String? = null,
    val bed: String? = null
)

@kotlinx.serialization.Serializable
@SerialName("update_times")
data class UpdateTimesConfig(
    val waveformIntervalInMillis: Int = 100,
    val generalReportIntervalInMillis: Int = 4_000,
    val descriptionUpdateIntervalInMillis: Int = 8_000
)