package org.somda.sdpi.test.v2.provider

import com.google.common.collect.Iterables
import com.google.common.util.concurrent.AbstractIdleService
import com.google.common.util.concurrent.Service
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.biceps.common.MdibDescriptionModifications
import org.somda.sdc.biceps.common.MdibEntity
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.access.WriteStateResult
import org.somda.sdc.biceps.common.storage.PreprocessingException
import org.somda.sdc.biceps.model.participant.*
import org.somda.sdc.biceps.model.participant.factory.CodedValueFactory
import org.somda.sdc.common.util.copyTyped
import org.somda.sdc.dpws.DpwsFramework
import org.somda.sdc.dpws.DpwsUtil
import org.somda.sdc.dpws.device.DeviceSettings
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingUtil
import org.somda.sdc.dpws.soap.wsaddressing.model.EndpointReferenceType
import org.somda.sdc.glue.common.FallbackInstanceIdentifier
import org.somda.sdc.glue.provider.factory.SdcDeviceFactory
import org.somda.sdc.glue.provider.plugin.SdcRequiredTypesAndScopes
import org.somda.sdpi.test.util.MdibLoader
import org.somda.sdpi.test.util.addMetricQualityDemo
import org.somda.sdpi.test.util.createLocalizedText
import org.somda.sdpi.test.v2.Handles
import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode
import java.net.InetAddress
import java.net.NetworkInterface
import java.time.Instant
import java.util.*
import java.util.stream.IntStream
import kotlin.math.sin


/**
 * Creates an instance of an SDC Provider.
 *
 * @param providerSetup options and configured injector
 */
class Provider(providerSetup: ProviderSetup) : DeviceSettings, AbstractIdleService() {
    private val injector = providerSetup.injector
    private val config = providerSetup.config

    private val epr = config.epr ?: ("urn:uuid:" + UUID.randomUUID().toString())

    private val networkInterface = checkNotNull(NetworkInterface.getByInetAddress(
        InetAddress.getByName(config.address)
    )) { "Could not determine network adapter using ip address ${config.address}" }

    private val mdibAccess = injector.getInstance(MdibLoader::class.java)
        .loadFromResourcePath("mdib_test_sequence_2.xml")

    private val operationHandler = OperationHandler(mdibAccess)

    private val dpwsFramework = injector.getInstance(DpwsFramework::class.java).apply {
        setNetworkInterface(networkInterface)
    }

    private val sdcDevice = injector.getInstance(SdcDeviceFactory::class.java).createSdcDevice(
        this,
        mdibAccess,
        operationHandler,
        setOf(injector.getInstance(SdcRequiredTypesAndScopes::class.java))
    )

    private var instanceIdentifier = InstanceIdentifier().apply {
        rootName = "http://www.somda.org"
        extensionName = "UNK"
    }

    private var currentLocation: LocationDetail? = null

    init {
        // bind to adapter matching ip
        logger.info { "Starting with address ${config.address}" }
    }

    override fun startUp() {
        val dpwsUtil = injector.getInstance(DpwsUtil::class.java)
        sdcDevice.hostingServiceAccess.setThisDevice(
            dpwsUtil.createDeviceBuilder()
                .setFriendlyName(
                    dpwsUtil.createLocalizedStrings()
                        .add("en", "SDPi Test Sequence Provider v2")
                        .get()
                )
                .setFirmwareVersion("v1.2.3")
                .setSerialNumber("1234-5678-9101-1121").get()
        )
        sdcDevice.hostingServiceAccess.setThisModel(
            dpwsUtil.createModelBuilder()
                .setManufacturer(
                    dpwsUtil.createLocalizedStrings()
                        .add("en", "Provider Example Inc.")
                        .add("de", "Beispiel Provider AG")
                        .add("cn", "范例公司")
                        .get()
                )
                .setManufacturerUrl("http://www.somda.org")
                .setModelName(
                    dpwsUtil.createLocalizedStrings()
                        .add("PEU")
                        .get()
                )
                .setModelNumber("54-32-1")
                .setPresentationUrl("http://www.somda.org")
                .get()
        )

        dpwsFramework.startAsync().awaitRunning()
        sdcDevice.startAsync().awaitRunning()
    }

    override fun shutDown() {
        sdcDevice.stopAsync().awaitTerminated()
        dpwsFramework.stopAsync().awaitTerminated()
        sdcDevice.stopAsync().awaitTerminated()
    }

    fun submitStateChanges(modifications: MdibStateModifications): WriteStateResult =
        mdibAccess.writeStates(modifications)

    fun setLocation(location: LocationDetail) {
        val newInstanceIdentifier = FallbackInstanceIdentifier.create(location)
        newInstanceIdentifier.ifPresent {
            instanceIdentifier = it
            logger.info { "Updated location context instance identifier to ${instanceIdentifier.rootName}" }
        }

        if (this.isRunning || state() === Service.State.STARTING) {
            logger.info("Updating location context")
            val stateModificationsList = mutableListOf<AbstractContextState>()
            val stateModifications = MdibStateModifications.Context(stateModificationsList)
            mdibAccess.startTransaction().use { readTransaction ->
                val locationContextDescriptor = readTransaction.getDescriptor(
                    Handles.LOCATION_CONTEXT_MDS_0,
                    LocationContextDescriptor::class.java
                ).orElseThrow {
                    RuntimeException("Could not find state for handle ${Handles.LOCATION_CONTEXT_MDS_0}")
                }
                stateModificationsList.add(LocationContextState().apply {
                    locationDetail = location
                    descriptorVersion = locationContextDescriptor.descriptorVersion
                    descriptorHandle = locationContextDescriptor.handle
                    stateVersion = BigInteger.ONE
                    handle = locationContextDescriptor.handle + "State"
                    bindingMdibVersion = mdibAccess.mdibVersion.version
                    contextAssociation = ContextAssociation.ASSOC
                    validator.add(instanceIdentifier)
                    identification.add(instanceIdentifier)
                })
            }
            mdibAccess.writeStates(stateModifications)
        }
        currentLocation = location.copyTyped()
    }

    /**
     * Adds a sine wave to the data of a waveform.
     */
    @Throws(PreprocessingException::class)
    fun changeWaveforms(
        modifications: MutableList<RealTimeSampleArrayMetricState>,
        time: Instant,
        handles: List<String>
    ) {
        mdibAccess.startTransaction().use { readTransaction ->
            for (handle in handles) {
                logger.debug { "Waveform handle: $handle" }
                val state = readTransaction.getState(handle, RealTimeSampleArrayMetricState::class.java).get()

                val minValue = 0
                val maxValue = 50
                val sampleCapacity = 100

                // sine wave
                val values = LinkedList<BigDecimal>()
                val delta = 2 * Math.PI / sampleCapacity
                IntStream.range(0, sampleCapacity)
                    .forEachOrdered { n: Int ->
                        values.add(
                            BigDecimal.valueOf((sin(n * delta) + 1) / 2.0 * (maxValue - minValue) + minValue)
                                .setScale(15, RoundingMode.DOWN)
                        )
                    }

                state.metricValue = SampleArrayValue().apply {
                    samples = values
                    determinationTime = time
                    addMetricQualityDemo(this)
                }
                modifications.add(state)
            }
        }
    }

    /**
     * Changes the components addressed by the given handles
     */
    @Throws(PreprocessingException::class)
    fun changeComponentStates(
        modifications: MutableList<AbstractDeviceComponentState>,
        handles: List<String>
    ) {
        for (handle in handles) {
            val entity = mdibAccess.getEntity(handle).get()
            modifications.add(
                when (entity.descriptor) {
                    is ClockDescriptor -> changeComponentState(entity.states.first() as ClockState)
                    is BatteryDescriptor -> changeComponentState(entity.states.first() as BatteryState)
                    is VmdDescriptor -> changeComponentState(entity.states.first() as VmdState)
                    is MdsDescriptor -> changeComponentState(entity.states.first() as MdsState)
                    else -> throw Exception("No support to change entity with handle $handle")
                }
            )
        }
    }

    /**
     * Changes the components addressed by the given handles
     */
    @Throws(PreprocessingException::class)
    fun changeOperationalStates(
        modifications: MutableList<AbstractOperationState>,
        handles: List<String>
    ) {
        for (handle in handles) {
            val entity = mdibAccess.getEntity(handle).get()
            modifications.add(
                when (entity.descriptor) {
                    is AbstractOperationDescriptor -> changeOperationState(entity.states.first() as AbstractOperationState)
                    else -> throw Exception("No support to change entity with handle $handle")
                }
            )
        }
    }

    private fun <T : AbstractOperationState> changeOperationState(state: T) = state.apply {
        operatingMode = if ((operatingMode ?: OperatingMode.DIS) == OperatingMode.DIS) {
            OperatingMode.EN
        } else {
            OperatingMode.DIS
        }
    }

    private fun <T : AbstractDeviceComponentState> changeComponentState(state: T) = state.apply {
        operatingCycles = operatingCycles?.inc() ?: 1
    }

    /**
     * Increments the value of a NumericMetricState.
     */
    @Throws(PreprocessingException::class)
    fun changeNumericMetrics(
        modifications: MutableList<AbstractMetricState>,
        time: Instant,
        handles: List<String>
    ) {
        for (handle in handles) {
            val state = mdibAccess.getState(handle, NumericMetricState::class.java).get()
            state.metricValue = NumericMetricValue().apply {
                value = if (state.metricValue != null && state.metricValue?.value != null) {
                    state.metricValue?.value?.add(BigDecimal.ONE)
                } else {
                    BigDecimal.ONE
                }
                determinationTime = time
                addMetricQualityDemo(this)
            }
            modifications.add(state)
        }
    }

    /**
     * Changes the content of a StringMetricState.
     */
    @Throws(PreprocessingException::class)
    fun changeStringMetrics(
        modifications: MutableList<AbstractMetricState>,
        time: Instant,
        handles: List<String>
    ) {
        for (handle in handles) {
            val state = mdibAccess.getState(handle, StringMetricState::class.java).get()
            state.metricValue = StringMetricValue().apply {
                value = UUID.randomUUID().toString()
                determinationTime = time
                addMetricQualityDemo(this)
            }
            modifications.add(state)
        }
    }

    /**
     * Changes the content of an EnumStringMetricState, selecting the next allowed value.
     */
    @Throws(PreprocessingException::class)
    fun changeEnumStringMetrics(
        modifications: MutableList<AbstractMetricState>,
        time: Instant,
        handles: List<String>
    ) {
        for (handle in handles) {
            val mdibEntity = mdibAccess.getEntity(handle).get()
            val descriptor = mdibEntity.descriptor as EnumStringMetricDescriptor
            val allowedValue = descriptor.allowedValue.map { it.value }.toList()
            val state = mdibEntity.states.first() as EnumStringMetricState
            state.metricValue = StringMetricValue().apply {
                if (state.metricValue != null && state.metricValue?.value != null) {
                    val iter = Iterables.cycle(allowedValue).iterator()
                    var next = iter.next()
                    // since Iterables.cycle by definition creates an infinite iterator,
                    // having a break condition is a safety mechanism
                    // this will either select the *next* allowed value or stop at some value
                    // It's by no means a good idea for a production solution, but kinda
                    // neat for this example.
                    var i = 0
                    while (iter.hasNext() && i < MAX_ENUM_ITERATIONS) {
                        if (next == state.metricValue?.value) {
                            next = iter.next()
                            break
                        }
                        next = iter.next()
                        i++
                    }
                    value = next
                } else {
                    value = allowedValue.first()
                }

                determinationTime = time
                addMetricQualityDemo(this)
            }
            modifications.add(state)
        }
    }

    /**
     * Toggles an AlertSignalState and AlertConditionState between presence on and off and performs a self-check.
     */
    fun changeAlertSignalAndConditionPresences(
        modifications: MutableList<AbstractAlertState>,
        time: Instant,
        signalHandles: List<String>
    ) {
        val affectedConditions = mutableListOf<MdibEntity>()
        for (signalHandle in signalHandles) {
            val signalEntity = mdibAccess.getEntity(signalHandle).get()
            val signalState = signalEntity.states.first() as AlertSignalState
            if (signalState.presence == null || signalState.presence == AlertSignalPresence.ON) {
                signalState.presence = AlertSignalPresence.OFF
            } else {
                signalState.presence = AlertSignalPresence.ON
            }

            val signalDescriptor = signalEntity.descriptor as AlertSignalDescriptor
            val conditionEntity = mdibAccess.getEntity(signalDescriptor.conditionSignaled).get().also {
                affectedConditions.add(it)
            }
            val conditionState = conditionEntity.states.first() as AlertConditionState
            conditionState.isPresence = signalState.presence == AlertSignalPresence.ON
            conditionState.determinationTime = time
            modifications.add(conditionState)
            modifications.add(signalState)
        }

        affectedConditions.fold(mutableSetOf<String>()) { acc, item ->
            acc.apply { add(item.parent.get()) }
        }.map {
            mdibAccess.getEntity(it).get()
        }.forEach {
            val systemState = it.states.first() as AlertSystemState
            systemState.lastSelfCheck = time
            systemState.selfCheckCount = systemState.selfCheckCount?.let { systemState.selfCheckCount?.inc() } ?: 1L
            systemState.presentTechnicalAlarmConditions = mutableListOf()
            systemState.presentPhysiologicalAlarmConditions = mutableListOf()
            for (condition in affectedConditions) {
                val descriptor = condition.descriptor as AlertConditionDescriptor
                if (condition.parent.get() != it.handle) {
                    continue
                }

                when (descriptor.kind) {
                    AlertConditionKind.PHY -> if ((condition.states.first() as AlertConditionState).isPresence == true) {
                        systemState.presentPhysiologicalAlarmConditions?.add(condition.handle)
                    }

                    AlertConditionKind.TEC -> if ((condition.states.first() as AlertConditionState).isPresence == true) {
                        systemState.presentTechnicalAlarmConditions?.add(condition.handle)
                    }

                    else -> Unit
                }
            }
            modifications.add(systemState)
        }
    }

    fun changeDescription() {
        MdibDescriptionModifications().also { modifications ->
            mdibAccess.startTransaction().use {
                descriptionChangeCounter++
                val condition = it.getEntity(Handles.ALERT_CONDITION_0_VMD_0_MDS_1).get()
                modifications.update(
                    org.somda.sdc.biceps.common.Pair.tryFromThrowing(
                        (condition.descriptor as AlertConditionDescriptor).apply {
                            type?.conceptDescription?.clear()
                            type?.conceptDescription?.add(createLocalizedText("Concept Description Change $descriptionChangeCounter"))
                            causeInfo.clear()
                            causeInfo.add(CauseInfo().apply {
                                description.add(createLocalizedText("Cause Description Change $descriptionChangeCounter"))
                                remedyInfo = RemedyInfo().apply {
                                    description.add(createLocalizedText("Remedy Description Change $descriptionChangeCounter"))
                                }
                            })
                        },
                        condition.states.first()
                    )
                )


                val metric = it.getEntity(Handles.NUMERIC_METRIC_0_CHANNEL_0_VMD_0_MDS_1).get()
                val newUnit = units[descriptionChangeCounter % 2]
                modifications.update(
                    org.somda.sdc.biceps.common.Pair.tryFromThrowing(
                        (metric.descriptor as NumericMetricDescriptor).apply {
                            unit = newUnit.first
                        },
                        (metric.states.first() as NumericMetricState).apply {
                            metricValue?.value?.let { value ->
                                metricValue!!.value = BigDecimal.valueOf(value.toDouble() * newUnit.second)
                            }
                        }
                    )
                )
            }
        }.also { mdibAccess.writeDescription(it) }


        val vmdHandle = "vmd_$descriptionChangeCounter.${Handles.MDS_1}"
        val channelHandle = "channel_$descriptionChangeCounter.$vmdHandle"
        val metricHandle = "numeric_metric_$descriptionChangeCounter.$channelHandle"

        MdibDescriptionModifications().also { modifications ->
            modifications.insert(
                org.somda.sdc.biceps.common.Pair.tryFromThrowing(
                    VmdDescriptor().apply {
                        handle = vmdHandle
                        type = dimless()
                    },
                    VmdState().apply {
                        descriptorHandle = vmdHandle
                    }
                ),
                Handles.MDS_1
            )
            modifications.insert(
                org.somda.sdc.biceps.common.Pair.tryFromThrowing(
                    ChannelDescriptor().apply {
                        handle = channelHandle
                        type = dimless()
                    },
                    ChannelState().apply {
                        descriptorHandle = channelHandle
                    }
                ),
                vmdHandle
            )
            modifications.insert(
                org.somda.sdc.biceps.common.Pair.tryFromThrowing(
                    NumericMetricDescriptor().apply {
                        handle = metricHandle
                        metricCategory = MetricCategory.SET
                        metricAvailability = MetricAvailability.INTR
                        type = dimless()
                        unit = dimless()
                        resolution = BigDecimal.ONE
                    },
                    NumericMetricState().apply {
                        descriptorHandle = metricHandle
                    }
                ),
                channelHandle
            )
        }.also { mdibAccess.writeDescription(it) }

        Thread.sleep(1000)

        MdibDescriptionModifications().apply {
            delete(metricHandle)
            delete(channelHandle)
            delete(vmdHandle)
        }.also { mdibAccess.writeDescription(it) }
    }

    companion object : Logging {
        private const val MAX_ENUM_ITERATIONS = 17

        var descriptionChangeCounter = 0

        private fun mlPerMin() = Pair(
            CodedValueFactory.createIeeeCodedValue("265234", "MDC_DIM_MILLI_L_PER_MIN"),
            60.0
        )

        private fun mlPerHr() = Pair(
            CodedValueFactory.createIeeeCodedValue("265266", "MDC_DIM_MILLI_L_PER_HR"),
            1 / 60.0
        )

        private fun dimless() = CodedValueFactory.createIeeeCodedValue("262656", "MDC_DIM_DIMLESS")

        val units = listOf(
            mlPerMin(),
            mlPerHr()
        )
    }

    override fun getEndpointReference(): EndpointReferenceType {
        return injector.getInstance(WsAddressingUtil::class.java).createEprWithAddress(epr)
    }

    override fun getNetworkInterface(): NetworkInterface {
        return networkInterface
    }
}