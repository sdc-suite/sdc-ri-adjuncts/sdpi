package org.somda.sdpi.test.v2.consumer

import kotlinx.serialization.SerialName
import org.somda.sdpi.test.util.config.TlsConfig

@kotlinx.serialization.Serializable
@SerialName("consumer")
data class ConsumerConfig(
    val epr: String? = null,
    val address: String? = null,
    val writeCommlog: Boolean = false,
    val tls: TlsConfig = TlsConfig()
)