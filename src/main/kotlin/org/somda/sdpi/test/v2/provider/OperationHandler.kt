package org.somda.sdpi.test.v2.provider

import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.access.MdibAccess
import org.somda.sdc.biceps.common.storage.PreprocessingException
import org.somda.sdc.biceps.model.message.*
import org.somda.sdc.biceps.model.message.InvocationError.OTH
import org.somda.sdc.biceps.model.participant.*
import org.somda.sdc.biceps.model.participant.ContextAssociation.NO
import org.somda.sdc.biceps.model.participant.EnumStringMetricDescriptor.AllowedValue
import org.somda.sdc.biceps.provider.access.LocalMdibAccess
import org.somda.sdc.glue.provider.sco.Context
import org.somda.sdc.glue.provider.sco.InvocationResponse
import org.somda.sdc.glue.provider.sco.OperationInvocationReceiver
import org.somda.sdpi.test.util.addMetricQualityDemo
import org.somda.sdpi.test.util.createLocalizedText
import org.somda.sdpi.test.v2.Handles
import java.math.BigDecimal
import java.time.Instant
import java.util.*
import java.util.function.Consumer
import kotlin.concurrent.thread


/**
 * This class provides a handler for incoming operations on the sdc provider.
 *
 * It implements generic handlers for some operations, which enables handling operations easily, although
 * a real application should be a little more specialized in its handling.
 */
class OperationHandler(private val mdibAccess: LocalMdibAccess) : OperationInvocationReceiver {

    private fun genericSetValue(context: Context, data: BigDecimal): InvocationResponse {
        val operationHandle = context.operationHandle
        logger.info { "Received SetValue request for $operationHandle: $data" }

        // find operation target
        val setNumeric = mdibAccess.getDescriptor(
            operationHandle,
            SetValueOperationDescriptor::class.java
        ).orElseThrow<RuntimeException> {
            val errorMessage = createLocalizedText("Operation target cannot be found")
            context.sendUnsuccessfulReport(
                InvocationState.FAIL, OTH, mutableListOf(errorMessage)
            )
            throw RuntimeException(String.format("Operation descriptor %s missing", operationHandle))
        }
        val operationTargetHandle = setNumeric.operationTarget
        val targetDesc = mdibAccess.getDescriptor(
            operationTargetHandle,
            NumericMetricDescriptor::class.java
        ).orElseThrow<RuntimeException> {
            val errorMessage = createLocalizedText("Operation target cannot be found")
            context.sendUnsuccessfulReport(InvocationState.FAIL, OTH, listOf(errorMessage))
            throw RuntimeException(
                String.format(
                    "Operation target descriptor %s missing",
                    operationTargetHandle
                )
            )
        }

        // find allowed range for descriptor and verify it's within
        targetDesc.technicalRange.forEach(
            Consumer { range: Range ->
                if (range.lower != null && range.lower?.compareTo(data)?.let { it > 0 } == true) {
                    // value too small
                    val errorMessage = createLocalizedText("Value too small")
                    context.sendUnsuccessfulReport(InvocationState.FAIL, OTH, listOf(errorMessage))
                    throw RuntimeException(
                        String.format(
                            "Operation set value below lower limit of %s, was %s",
                            range.lower, data
                        )
                    )
                }
                if (range.upper != null && range.upper?.compareTo(data)?.let { it < 0 } == true) {
                    // value too big
                    val errorMessage = createLocalizedText("Value too big")
                    context.sendUnsuccessfulReport(InvocationState.FAIL, OTH, listOf(errorMessage))
                    throw RuntimeException(
                        String.format(
                            "Operation set value below lower limit of %s, was %s",
                            range.lower, data
                        )
                    )
                }
            }
        )
        val targetState = mdibAccess.getState(
            operationTargetHandle,
            NumericMetricState::class.java
        ).orElseThrow<RuntimeException> {
            val errorMessage = createLocalizedText("Operation target state cannot be found")
            context.sendUnsuccessfulReport(InvocationState.FAIL, OTH, listOf(errorMessage))
            throw RuntimeException(
                String.format(
                    "Operation target descriptor %s missing",
                    operationTargetHandle
                )
            )
        }
        if (targetState.metricValue == null) {
            targetState.metricValue = NumericMetricValue()
        }
        targetState.metricValue?.value = data
        targetState.metricValue?.determinationTime = Instant.now()
        addMetricQualityDemo(targetState.metricValue)
        val modList = mutableListOf<AbstractMetricState>()
        val mod = MdibStateModifications.Metric(modList)
        modList.add(targetState)
        return try {
            mdibAccess.writeStates(mod)
            context.sendSuccessfulReport(InvocationState.FIN)
            context.createSuccessfulResponse(InvocationState.FIN)
        } catch (e: PreprocessingException) {
            logger.error(e) { "Error while writing states" }
            val errorMessage = createLocalizedText("Error while writing states")
            context.sendUnsuccessfulReport(
                InvocationState.FAIL,
                InvocationError.UNSPEC,
                mutableListOf(errorMessage)
            )
            context.createUnsuccessfulResponse(
                InvocationState.FAIL, InvocationError.UNSPEC, mutableListOf(errorMessage)
            )
        }
    }

    private fun genericSetString(context: Context, data: String, isEnumString: Boolean): InvocationResponse {
        // TODO: Check if state is modifiable
        context.sendSuccessfulReport(InvocationState.WAIT)
        val response = context.createSuccessfulResponse(InvocationState.WAIT)

        thread(isDaemon = true) {
            context.sendSuccessfulReport(InvocationState.START)
            val operationHandle = context.operationHandle
            logger.info { "Received SetString for $operationHandle: $data" }

            // find operation target
            val setString = mdibAccess.getDescriptor(
                operationHandle,
                SetStringOperationDescriptor::class.java
            ).orElseThrow<RuntimeException> {
                val errorMessage = createLocalizedText("Operation target cannot be found")
                context.sendUnsuccessfulReport(
                    InvocationState.FAIL, OTH, mutableListOf(errorMessage)
                )
                throw RuntimeException(String.format("Operation descriptor %s missing", operationHandle))
            }
            val operationTargetHandle = setString.operationTarget

            // verify if new data is allowed for enum strings
            if (isEnumString) {
                val targetDesc = mdibAccess.getDescriptor(
                    operationTargetHandle,
                    EnumStringMetricDescriptor::class.java
                ).orElseThrow<RuntimeException> {
                    val errorMessage = createLocalizedText("Operation target descriptor cannot be found")
                    context.sendUnsuccessfulReport(
                        InvocationState.FAIL,
                        OTH,
                        mutableListOf(errorMessage)
                    )
                    throw RuntimeException(
                        "Operation target descriptor $operationTargetHandle missing",
                    )
                }

                // validate data is allowed
                val first = targetDesc.allowedValue.stream().filter { x: AllowedValue -> x.value == data }.findFirst()
                if (first.isEmpty) {
                    // not allowed value, bye bye
                    val errormessage = createLocalizedText("Value is not allowed here")
                    context.sendUnsuccessfulReport(
                        mdibAccess.mdibVersion,
                        InvocationState.FAIL,
                        InvocationError.UNSPEC,
                        mutableListOf(errormessage)
                    )
                    throw RuntimeException(
                        "Value is not allowed here $operationTargetHandle missing",
                    )
                }
            }
            val targetState = mdibAccess.getState(
                operationTargetHandle,
                StringMetricState::class.java
            ).orElseThrow<RuntimeException> {
                val errorMessage = createLocalizedText("Operation target state cannot be found")
                context.sendUnsuccessfulReport(
                    InvocationState.FAIL,
                    OTH,
                    mutableListOf(errorMessage)
                )
                throw RuntimeException(
                    "Operation target descriptor $operationTargetHandle missing",
                )
            }
            if (targetState.metricValue == null) {
                targetState.metricValue = StringMetricValue()
            }
            targetState.metricValue?.value = data
            targetState.metricValue?.determinationTime = Instant.now()
            addMetricQualityDemo(targetState.metricValue)
            val modList = mutableListOf<AbstractMetricState>()
            val mod = MdibStateModifications.Metric(modList)
            modList.add(targetState)
            try {
                mdibAccess.writeStates(mod)
                context.sendSuccessfulReport(InvocationState.FIN, targetState.descriptorHandle)
            } catch (e: PreprocessingException) {
                logger.error(e) { "Error while writing states" }
                val errorMessage = createLocalizedText("Error while writing states")
                context.sendUnsuccessfulReport(
                    InvocationState.FAIL,
                    InvocationError.UNSPEC,
                    mutableListOf(errorMessage)
                )
            }
        }
        return response
    }

    override fun handleSetContextState(
        context: Context,
        operationHandle: String,
        setContextState: SetContextState
    ): InvocationResponse {
        return if (operationHandle == Handles.SET_CONTEXT_0_SCO_MDS_0) {
            setContextState(context, setContextState.proposedContextState.filterIsInstance<PatientContextState>())
        } else {
            super.handleSetContextState(context, operationHandle, setContextState)
        }
    }

    private fun setContextState(context: Context, proposedStates: List<PatientContextState>): InvocationResponse {
        synchronized(this) {
            logger.info { "Received SetContextState for ${context.operationHandle}" }

            if (proposedStates.size != 1) {
                val msg = listOf(
                    createLocalizedText(
                        "Expected exactly 1 patient context state, found ${proposedStates.size}"
                    )
                )
                context.sendUnsuccessfulReport(InvocationState.FAIL, OTH, msg)
                return context.createUnsuccessfulResponse(InvocationState.FAIL, OTH, msg)
            }

            val assoc = proposedStates.first().contextAssociation ?: NO
            if (assoc == NO) {
                val errMsg = listOf(
                    LocalizedText().apply {
                        lang = "en"
                        value = "Context to set was not-associated"
                    }
                )
                context.sendUnsuccessfulReport(InvocationState.FAIL, OTH, errMsg)
                return context.createUnsuccessfulResponse(InvocationState.FAIL, OTH, errMsg)
            }

            val modificationsList = mutableListOf<AbstractContextState>()
            val modifications = MdibStateModifications.Context(modificationsList)
            val newHandle = "patient_context_" + UUID.randomUUID().toString()
            context.mdibAccess.startTransaction().use {
                val patientContext = it.getEntity(Handles.PATIENT_CONTEXT_MDS_0).get()
                val contextStates = patientContext.getStates(PatientContextState::class.java)
                for (contextState in contextStates) {
                    if (contextState.contextAssociation == ContextAssociation.DIS) {
                        contextState.contextAssociation = NO
                        modificationsList.add(contextState)
                        continue
                    }

                    if (contextState.contextAssociation == ContextAssociation.ASSOC) {
                        contextState.contextAssociation = ContextAssociation.DIS
                        modificationsList.add(contextState)
                        continue
                    }
                }

                modificationsList.add(proposedStates.first().apply {
                    handle = newHandle
                    descriptorHandle = Handles.PATIENT_CONTEXT_MDS_0
                    descriptorVersion = patientContext.descriptor.descriptorVersion
                })
            }

            val mdibVersion = context.mdibAccess.writeStates(modifications).mdibVersion

            context.sendSuccessfulReport(mdibVersion, InvocationState.FIN, newHandle)
            return context.createSuccessfulResponse(mdibVersion, InvocationState.FIN)
        }
    }

    override fun handleSetMetricState(
        context: Context,
        operationHandle: String,
        setMetricState: SetMetricState
    ): InvocationResponse {
        return if (operationHandle == Handles.SET_METRIC_0_SCO_VMD_1_MDS_0) {
            if (setMetricState.proposedMetricState.size != 2) {
                context.createUnsuccessfulResponse(InvocationState.FAIL, OTH, listOf(
                    LocalizedText().apply {
                        lang = "en"
                        value = "Proposed metric states list size is expected to be 2, was " +
                                "${setMetricState.proposedMetricState.size}"
                    }
                ))
            } else {
                val metric1 = setMetricState.proposedMetricState[0]
                val metric2 = setMetricState.proposedMetricState[1]
                val states = mdibAccess.startTransaction().use { rt ->
                    listOf(setMetric(rt, metric1), setMetric(rt, metric2))
                }
                mdibAccess.writeStates(MdibStateModifications.Metric(states)).mdibVersion.let {
                    context.sendSuccessfulReport(it, InvocationState.FIN)
                    context.createSuccessfulResponse(it, InvocationState.FIN)
                }
            }
        } else {
            super.handleSetMetricState(context, operationHandle, setMetricState)
        }
    }

    private fun setMetric(mdibAccess: MdibAccess, metric: AbstractMetricState): AbstractMetricState {
        return when (metric) {
            is NumericMetricState -> {
                val metric1FromMdib = mdibAccess.getState(metric.descriptorHandle, NumericMetricState::class.java)
                metric1FromMdib.orElseThrow().apply {
                    metricValue = metric.metricValue
                }
            }

            is StringMetricState -> {
                val metric1FromMdib = mdibAccess.getState(metric.descriptorHandle, StringMetricState::class.java)
                metric1FromMdib.orElseThrow().apply {
                    metricValue = metric.metricValue
                }
            }

            else -> throw RuntimeException("Unrecognized metric type for set-metric: ${metric::class.java}")
        }
    }

    override fun handleSetValue(context: Context, operationHandle: String, setValue: SetValue): InvocationResponse {
        return if (operationHandle == Handles.SET_VALUE_0_SCO_MDS_0) {
            genericSetValue(context, setValue.requestedNumericValue)
        } else {
            super.handleSetValue(context, operationHandle, setValue)
        }
    }

    override fun handleSetString(context: Context, operationHandle: String, setString: SetString): InvocationResponse {
        return if (operationHandle == Handles.SET_STRING_0_SCO_MDS_0) {
            genericSetString(context, setString.requestedStringValue, true)
        } else {
            super.handleSetString(context, operationHandle, setString)
        }
    }

//
//    @IncomingSetServiceRequest(operationHandle = Handles.HANDLE_ACTIVATE, listType = String::class)
//    fun activateExample(context: Context, args: List<String?>?): InvocationResponse {
//        context.sendSuccessfulReport(InvocationState.START)
//        LOG.info("Received Activate for {}", Handles.HANDLE_ACTIVATE)
//        context.sendSuccessfulReport(InvocationState.FIN)
//        return context.createSuccessfulResponse(mdibAccess.mdibVersion, InvocationState.FIN)
//    }
//
//    @IncomingSetServiceRequest(operationHandle = "actop.mds0_sco_0", listType = String::class)
//    fun activateExample2(context: Context, args: List<String?>?): InvocationResponse {
//        context.sendSuccessfulReport(InvocationState.START)
//        LOG.info("Received Activate for {}", context.operationHandle)
//        context.sendSuccessfulReport(InvocationState.FIN)
//        return context.createSuccessfulResponse(mdibAccess.mdibVersion, InvocationState.FIN)
//    }

    companion object : Logging
}
