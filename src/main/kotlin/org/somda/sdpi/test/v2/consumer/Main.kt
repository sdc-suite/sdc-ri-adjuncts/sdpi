package org.somda.sdpi.test.v2.consumer

import org.somda.sdpi.test.util.AppRunner
import java.util.concurrent.TimeUnit
import kotlin.system.exitProcess

fun main(args: Array<String>) = Main().main(args)

class Main : AppRunner() {
    override fun run() {
        val consumerSetup = ConsumerSetup(this)
        val consumer = Consumer(consumerSetup)

        consumer.startAsync().awaitRunning()

        consumer.runTests()

        Runtime.getRuntime().addShutdownHook(Thread {
            for (test in Tests.toList()) {
                val msg = if (test.result == Test.Result.FAILED) {
                    " with message: ${test.message}"
                } else {
                    ""
                }
                println("### Test ${test.step} ### ${test.result} $msg")
            }
        })

        consumer.stopAsync().awaitTerminated(10, TimeUnit.SECONDS)

        val hasFail = Tests.toList()
            .any { it.result == Test.Result.FAILED }

        if (hasFail) {
            exitProcess(1)
        }
    }
}