package org.somda.sdpi.test.v2.provider

import com.akuleshov7.ktoml.Toml
import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Injector
import com.google.inject.assistedinject.FactoryModuleBuilder
import com.google.inject.util.Modules
import kotlinx.serialization.serializer
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.biceps.common.CommonConstants
import org.somda.sdc.biceps.guice.DefaultBicepsConfigModule
import org.somda.sdc.biceps.guice.DefaultBicepsModule
import org.somda.sdc.common.guice.DefaultCommonConfigModule
import org.somda.sdc.common.guice.DefaultCommonModule
import org.somda.sdc.dpws.CommunicationLog
import org.somda.sdc.dpws.CommunicationLogImpl
import org.somda.sdc.dpws.DpwsConfig
import org.somda.sdc.dpws.crypto.CryptoConfig
import org.somda.sdc.dpws.crypto.CryptoSettings
import org.somda.sdc.dpws.factory.CommunicationLogFactory
import org.somda.sdc.dpws.guice.DefaultDpwsModule
import org.somda.sdc.dpws.soap.SoapConfig
import org.somda.sdc.dpws.soap.wseventing.WsEventingConfig
import org.somda.sdc.glue.GlueConstants
import org.somda.sdc.glue.guice.DefaultGlueConfigModule
import org.somda.sdc.glue.guice.DefaultGlueModule
import org.somda.sdc.glue.guice.GlueDpwsConfigModule
import org.somda.sdpi.test.util.AppRunner
import org.somda.sdpi.test.v2.Constants
import java.net.URI
import java.security.cert.X509Certificate
import java.time.Duration
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession

val SDC_SERVICE_CONSUMER_ROLE: URI = URI.create(
    GlueConstants.OID_KEY_PURPOSE_SDC_SERVICE_CONSUMER
)

class ProviderSetup(app: AppRunner) {
    private val tomlContent = app.configFile.readText()

    val config = Toml.partiallyDecodeFromString<ProviderConfig>(serializer(), tomlContent, "provider")
        // command line overrides
        .let { app.adapterIp?.let { ip -> it.copy(address = ip) } ?: it }
        .let { app.epr?.let { epr -> it.copy(epr = epr) }  ?: it }

    val injector: Injector

    companion object : Logging

    init {
        Configurator.reconfigure(app.localLoggerConfig(Level.INFO))

        injector = Guice.createInjector(
            DefaultCommonConfigModule(),
            DefaultGlueModule(),
            DefaultGlueConfigModule(),
            DefaultBicepsModule(),
            DefaultBicepsConfigModule(),
            DefaultCommonModule(),
            Modules.override(
                DefaultDpwsModule()
            ).with(object : AbstractModule() {
                override fun configure() {
                    if (config.writeCommlog) {
                        install(
                            FactoryModuleBuilder()
                                .implement(CommunicationLog::class.java, CommunicationLogImpl::class.java)
                                .build(CommunicationLogFactory::class.java)
                        )
                    }
                }
            }),
            object : GlueDpwsConfigModule() {
                override fun customConfigure() {
                    // can't call this as we cannot override JAXB_CONTEXT_PATH otherwise
                    // super.customConfigure()

                    // add extension stuff
                    bind(
                        WsEventingConfig.SOURCE_MAX_EXPIRES,
                        Duration::class.java,
                        Duration.ofSeconds(15)
                    )
                    bind(
                        SoapConfig.JAXB_CONTEXT_PATH,
                        String::class.java,
                        CommonConstants.BICEPS_JAXB_CONTEXT_PATH + ":org.somda.sdpi.model"
                    );
                    bind(
                        SoapConfig.JAXB_SCHEMA_PATH,
                        String::class.java,
                        GlueConstants.SCHEMA_PATH + ":SdpiExtensions.xsd"
                    )
                    bind(
                        SoapConfig.NAMESPACE_MAPPINGS,
                        String::class.java,
                        org.somda.sdc.glue.common.CommonConstants.NAMESPACE_PREFIX_MAPPINGS_MDPWS +
                                org.somda.sdc.glue.common.CommonConstants.NAMESPACE_PREFIX_MAPPINGS_BICEPS +
                                org.somda.sdc.glue.common.CommonConstants.NAMESPACE_PREFIX_MAPPINGS_GLUE +
                                Constants.NAMESPACE_PREFIX_MAPPING_SDPI
                    )
                    if (!config.tls.disableTls) {
                        bind(
                            CryptoConfig.CRYPTO_SETTINGS,
                            CryptoSettings::class.java,
                            app.createCustomCryptoSettings(config.tls)
                        )
                    }
                    bind(DpwsConfig.HTTPS_SUPPORT, Boolean::class.java, !config.tls.disableTls)
                    bind(DpwsConfig.HTTP_SUPPORT, Boolean::class.java, config.tls.disableTls)
                    bind(CryptoConfig.CRYPTO_DEVICE_HOSTNAME_VERIFIER,
                        HostnameVerifier::class.java,
                        HostnameVerifier bind@{ _: String?, session: SSLSession ->
                            try {
                                // since this is not a real implementation, we still want to allow all peers
                                // which is why this doesn't really filter anything
                                // returning false in this filter would reject an incoming request
                                val peerCerts = session.peerCertificates
                                val x509 = peerCerts[0] as X509Certificate
                                val extendedKeyUsage = x509.extendedKeyUsage
                                if (extendedKeyUsage == null || extendedKeyUsage.isEmpty()) {
                                    logger.warn { "No EKU in peer certificate" }
                                    return@bind true
                                }

                                // find matching provider key purpose
                                for (key in extendedKeyUsage) {
                                    try {
                                        val keyUri = URI.create(key)
                                        if (keyUri == SDC_SERVICE_CONSUMER_ROLE) {
                                            logger.debug { "SDC Service Consumer PKP found" }
                                            return@bind true
                                        }
                                    } catch (e: IllegalArgumentException) {
                                        // don't care, was no uri
                                    }
                                }
                                return@bind true
                            } catch (e: Exception) {
                                logger.error(e) { "Error while validating client certificate" }
                            }
                            false
                        })
                }
            })
    }
}